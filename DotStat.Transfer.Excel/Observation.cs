﻿using System.Diagnostics.CodeAnalysis;

namespace DotStat.Transfer.Excel
{
    /// <summary>
    /// An observation record. The coordinates must be all non-null strings that can uniquely identify an observation
    /// The data field will hold a list of values for all observation level attributes. The positions in both the coordinates and the Data segments
    /// are defined within the dataset definition. A null value means non-existing value.
    /// The order of columns is as follows: 
    /// - dimensions
    /// - production attributes
    /// - imported attributes
    /// </summary>

    [ExcludeFromCodeCoverage]
    public class Observation
    {
        //todo: this need to be segmented into two: Coordinates and Data for simpler working with optional attributes
        public readonly string[] Record;

        //todo: this field must be on IOResult class
        public readonly string Source;

        public Observation(string[] record, string source)
        {
            Record = record;
            Source = source;
        }
    }
}