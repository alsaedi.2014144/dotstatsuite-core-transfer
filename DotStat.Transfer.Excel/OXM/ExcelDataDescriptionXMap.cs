﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using DotStat.Domain;
using DotStat.MappingStore;
using DotStat.Transfer.Excel.Excel;
using DotStat.Transfer.Excel.Exceptions;
using DotStat.Transfer.Excel.Mapping;
using DotStat.Transfer.Excel.Util;
using OXM;

namespace DotStat.Transfer.Excel.OXM
{
    internal class MatchPatternXMap : ClassMap<MatchPattern>
    {
        private bool _IsExpression;
        private bool _IsRegex;
        private string _Name;
        private string _Condition;

        public MatchPatternXMap()
        {
            Map(m => !m.IsCondition ? m.Pattern : null)
                .ToAttribute("Name", false)
                .Set(name => _Name = name)
                .Converter(new StringConverter());

            Map(m => m.IsRegex)
                .ToAttribute("IsRegex", false, "false")
                .Set(isregex => _IsRegex = isregex)
                .Converter(new BooleanConverter());

            Map(m => m.IsExpression && !m.IsCondition)
                .ToAttribute("IsExpression", false, "false")
                .Set(isex => _IsExpression = isex)
                .Converter(new BooleanConverter());

            Map(m => m.IsCondition ? m.Pattern : null)
                .ToAttribute("Condition", false)
                .Set(cond => _Condition = cond)
                .Converter(new StringConverter());
        }


        protected override MatchPattern Return()
        {
            if (_Name != null && _Condition != null)
            {
                throw new ApplicationArgumentException(
                    "Name and Condition attributes are mutually exclusive: {0} - {1}".F(_Name, _Condition));
            }

            return new MatchPattern(
                _Name ?? _Condition,
                _IsRegex,
                _IsExpression || _Condition != null,
                _Condition != null,
                Source);
        }
    }

    internal class AxisRefenceXMap : ClassMap<AxisReferenceExpression>
    {
        private string _Index;
        private bool _IsExpression;

        public AxisRefenceXMap()
        {
            Map(m => m.AxisExpression)
                .ToAttribute("Index", true)
                .Set(coord => _Index = coord)
                .Converter(new StringConverter());

            Map(m => m.IsExpression)
                .ToAttribute("IsExpression", false, "false")
                .Set(ie => _IsExpression = ie)
                .Converter(new BooleanConverter());
        }

        protected override AxisReferenceExpression Return()
        {
            return new AxisReferenceExpression(_Index, Source, _IsExpression);
        }
    }

    internal class TargetWorkbookReferenceXMap : ClassMap<BreakdownDimensionExpression>
    {
        private readonly ExcelCoordMappingDescriptorXMap _Owner;
        private string _TargetWorkbookExpr;
        private readonly SortedDictionary<int, string> _DimensionCodes;

        public TargetWorkbookReferenceXMap(ExcelCoordMappingDescriptorXMap owner)
        {
            _Owner = owner;
            _DimensionCodes = new SortedDictionary<int, string>();
            Map(m => m.ValueExpression)
                .ToAttribute("TargetWorkbook", true)
                .Set(expr => _TargetWorkbookExpr = expr)
                .Converter(new StringConverter());

            Map(m => m.BreakdownDimensions[0])
                .ToAttribute("DimensionCode", true)
                .Set(ve => _DimensionCodes.Add(0, ve))
                .Converter(new StringConverter());

            Map(m => m.BreakdownDimensions.Length > 1 ? m.BreakdownDimensions[1] : null)
                .ToAttribute("DimensionCode1", false)
                .Set(ve => _DimensionCodes.Add(1, ve))
                .Converter(new StringConverter());

            Map(m => m.BreakdownDimensions.Length > 2 ? m.BreakdownDimensions[2] : null)
                .ToAttribute("DimensionCode2", false)
                .Set(ve => _DimensionCodes.Add(2, ve))
                .Converter(new StringConverter());

            Map(m => m.BreakdownDimensions.Length > 3 ? m.BreakdownDimensions[3] : null)
                .ToAttribute("DimensionCode3", false)
                .Set(ve => _DimensionCodes.Add(3, ve))
                .Converter(new StringConverter());
        }

        protected override BreakdownDimensionExpression Return()
        {
            throw new NotImplementedException();

            //var ds = _Owner.GetDataset( );
            //var err = string.Join(
            //        ",",
            //        _DimensionCodes.Where( dc => ds.FindDimension( dc.Value, DbLanguage.Code ) == null ).
            //                        Select( dc => dc.Value ) );

            //if ( err.Length > 0 ) {
            //    throw new ApplicationArgumentException( "The following dimension codes are not found in the dataset {0}: {1}".F( ds.FullCode, err ) );
            //}

            //return new BreakdownDimensionExpression( _TargetWorkbookExpr, Source, _DimensionCodes.Values.ToArray( ) );
        }
    }

    internal class SdmxArtifactCellReferenceExpressionXMap : ClassMap<SdmxArtifactCellReferenceExpression>
    {
        private string _code;
        private readonly SortedDictionary<int, string> _valueExpr;
        private readonly SortedDictionary<int, string> _coordExpr;
        private bool _isExpression;

        public SdmxArtifactCellReferenceExpressionXMap(bool isCodeRequired = true)
        {
            _valueExpr = new SortedDictionary<int, string>();
            _coordExpr = new SortedDictionary<int, string>();

            Map(m => m.Code)
                .ToAttribute("Code", isCodeRequired)
                .Set(code => _code = code)
                .Converter(new StringConverter());

            Map(m => m.IsExpression)
                .ToAttribute("IsExpression", false, "true")
                .Set(v => _isExpression = v)
                .Converter(new BooleanConverter());

            for (var ii = 0; ii < 10; ii++)
            {
                var suffix = ii == 0 ? "" : ii.ToString();
                var idx = ii;
                Map(m => m.ValueExpressionList.Count > idx ? m.ValueExpressionList[idx] : null)
                    .ToAttribute("Value" + suffix, false)
                    .Set(ve => _valueExpr.Add(idx, ve))
                    .Converter(new StringConverter());

                Map(m => m.CellReferenceListResult.Count > idx ? m.CellReferenceListResult[idx].CoordExpression : null)
                    .ToAttribute("Coord" + suffix, false)
                    .Set(ve => _coordExpr.Add(idx, ve))
                    .Converter(new StringConverter());
            }
        }

        protected override SdmxArtifactCellReferenceExpression Return()
        {
            return new SdmxArtifactCellReferenceExpression(
                _code ?? "-", 
                _valueExpr.Values.ToList(),
                _coordExpr.Values.ToList(), 
                Source, 
                _isExpression
            );
        }
    }

    internal class CellValueRefenceXMap : ClassMap<ValueCellReferenceExpression>
    {
        protected string Coord;
        protected string Value;
        protected bool IsExpression;
        protected string MemberMappingRef;


        public CellValueRefenceXMap(bool defaultIsExpression = false)
        {
            IsExpression = defaultIsExpression;

            Map(m => m.CoordExpression)
                .ToAttribute("Coord", false)
                .Set(coord => Coord = coord)
                .Converter(new StringConverter());

            Map(m => m.ValueExpression)
                .ToAttribute("Value", false)
                .Set(value => Value = value)
                .Converter(new StringConverter());

            Map(m => m.IsExpression)
                .ToAttribute("IsExpression", false, defaultIsExpression ? "true" : "false")
                .Set(ie => IsExpression = ie)
                .Converter(new BooleanConverter());

            Map(m => m.MemberMapperName)
                .ToAttribute("MemberMappingRef", false)
                .Set(vm => MemberMappingRef = vm)
                .Converter(new StringConverter());
        }

        protected override ValueCellReferenceExpression Return()
        {
            return new ValueCellReferenceExpression(Value, Coord, exprSource: Source, isExpression: IsExpression,
                memberMapperName: MemberMappingRef);
        }
    }

    internal class CellRefenceXMap : ClassMap<CellReferenceExpression>
    {
        protected string Coord;
        protected bool IsExpression;


        public CellRefenceXMap(bool defaultIsExpression = false)
        {
            IsExpression = defaultIsExpression;

            Map(m => m.CoordExpression)
                .ToAttribute("Coord", true)
                .Set(coord => Coord = coord)
                .Converter(new StringConverter());


            Map(m => m.IsExpression)
                .ToAttribute("IsExpression", false, defaultIsExpression ? "true" : "false")
                .Set(ie => IsExpression = ie)
                .Converter(new BooleanConverter());
        }

        protected override CellReferenceExpression Return()
        {
            return new CellReferenceExpression(Coord, Source, IsExpression);
        }
    }

    internal class ExcludedCellRefXMap : ClassMap<ExcludedCellRefXMap.ExcludedCell>
    {
        internal class ExcludedCell
        {
            public bool IsExpression;
            public string CoordExpression;
            public string ConditionExpression;
            public string Source;
        }

        private readonly ExcludedCell _Res;

        public ExcludedCellRefXMap()
        {
            _Res = new ExcludedCell();

            Map(m => m.CoordExpression)
                .ToAttribute("Coord", false)
                .Set(coord => _Res.CoordExpression = coord)
                .Converter(new StringConverter());

            Map(m => m.ConditionExpression)
                .ToAttribute("Condition", false)
                .Set(cond => _Res.ConditionExpression = cond)
                .Converter(new StringConverter());

            Map(m => m.IsExpression && m.ConditionExpression == null)
                .ToAttribute("IsExpression", false, "false")
                .Set(ie => _Res.IsExpression = ie)
                .Converter(new BooleanConverter());
        }

        protected override ExcludedCell Return()
        {
            _Res.Source = Source;
            return _Res;
        }
    }

    internal class ExcelCoordMappingDescriptorXMap : ClassMap<ExcelCoordMappingDescriptor>
    {
        private readonly Dataflow _dataflow;
        private readonly List<MatchPattern> _Worksheets;

        private string _Name;
        private string _DataMappingName;
        private string _ValueMapperName;
        private bool _ExcludeNulls;
        private BreakdownDimensionExpression _TargetWorkbookExpression;
        private CellReferenceExpression _TopLeft;
        private CellReferenceExpression _BottomRight;
        private readonly List<AxisReferenceExpression> _ExcludedRows;
        private readonly List<AxisReferenceExpression> _ExcludedColumns;
        private readonly List<ExcludedCellRefXMap.ExcludedCell> _ExcludedCells;
        private readonly List<SdmxArtifactCellReferenceExpression> _DimensionCells;
        private readonly List<SdmxArtifactCellReferenceExpression> _ObsAttributeCells;

        private ValueCellReferenceExpression _PrimaryValueExpr;
        private bool _IsActive;


        public ExcelCoordMappingDescriptorXMap(Dataflow dataflow)
        {
            _dataflow = dataflow;
            _Worksheets = new List<MatchPattern>();
            _DimensionCells = new List<SdmxArtifactCellReferenceExpression>();
            _ObsAttributeCells = new List<SdmxArtifactCellReferenceExpression>();

            _ExcludedCells = new List<ExcludedCellRefXMap.ExcludedCell>();
            _ExcludedRows = new List<AxisReferenceExpression>();
            _ExcludedColumns = new List<AxisReferenceExpression>();

            Map(m => m.Name)
                .ToAttribute("Name", true)
                .Set(name => _Name = name)
                .Converter(new StringConverter());



            Map(m => m.Mapper != null ? m.Mapper.Name : null)
                .ToAttribute("MappingRef", false)
                .Set(name => _DataMappingName = name)
                .Converter(new StringConverter());

            Map(m => m.ValueMapperName)
                .ToAttribute("ValueMappingRef", false)
                .Set(name => _ValueMapperName = name)
                .Converter(new StringConverter());

            Map(m => m.ExcludeNulls)
                .ToAttribute("ExcludeNulls", false, "false")
                .Set(en => _ExcludeNulls = en)
                .Converter(new BooleanConverter());

            Map(m => m.IsActive)
                .ToAttribute("IsActive", false, "true")
                .Set(ia => _IsActive = ia)
                .Converter(new BooleanConverter());

            Map(m => m.TargetWorkbookExpressionExpression)
                .ToElement("Export-Breakdown", false)
                .Set(twe => _TargetWorkbookExpression = twe)
                .ClassMap(() => new TargetWorkbookReferenceXMap(this));

            MapContainer("Worksheets", true)
                .MapCollection(m => m.Worksheets)
                .ToElement("Worksheet", true)
                .Set(sheet => _Worksheets.Add(sheet))
                .ClassMap(() => new MatchPatternXMap());

            Map(m => m.TopLeft)
                .ToElement("Top-Left", true)
                .Set(cell => _TopLeft = cell)
                .ClassMap(() => new CellRefenceXMap());

            Map(m => m.BottomRight)
                .ToElement("Bottom-Right", true)
                .Set(cell => _BottomRight = cell)
                .ClassMap(() => new CellRefenceXMap());

            MapCollection(m => m.DimensionExpressions)
                .ToElement("Dimension", false)
                .Set(dc => _DimensionCells.Add(dc))
                .ClassMap(() => new SdmxArtifactCellReferenceExpressionXMap());

            MapCollection(m => m.ObsAttributeExpressions)
                .ToElement("ObsAttribute", false)
                .Set(dc => _ObsAttributeCells.Add(dc))
                .ClassMap(() => new SdmxArtifactCellReferenceExpressionXMap());

            var mc = MapContainer("Exclude", false);
            mc.MapCollection(m => m.ExcludedRows)
                .ToElement("Row", false)
                .Set(row => _ExcludedRows.Add(row))
                .ClassMap(() => new AxisRefenceXMap());

            mc.MapCollection(m => m.ExcludedColumns)
                .ToElement("Column", false)
                .Set(col => _ExcludedColumns.Add(col))
                .ClassMap(() => new AxisRefenceXMap());

            mc.MapCollection
                (m => m.ExcludedCells.Select
                    (ec => new ExcludedCellRefXMap.ExcludedCell
                    {
                        CoordExpression = ec.CoordExpression,
                        IsExpression = ec.IsExpression
                    })
                    .Concat
                    (m.ExcludeCellConditions.Select
                        (ec => new ExcludedCellRefXMap.ExcludedCell
                        {
                            ConditionExpression = ec.Condition
                        })))
                .ToElement("Cell", false)
                .Set(cell => _ExcludedCells.Add(cell))
                .ClassMap(() => new ExcludedCellRefXMap());

            Map(m => m.PrimaryValueExpression)
                .ToElement("Data-Value", false)
                .Set(v => _PrimaryValueExpr = v)
                .ClassMap(() => new CellValueRefenceXMap(true));
        }



        protected override ExcelCoordMappingDescriptor Return()
        {
            return new ExcelCoordMappingDescriptor
                (_Name,
                    _dataflow,
                    _Worksheets,
                    _TopLeft,
                    _BottomRight,
                    _ExcludeNulls,
                    _DataMappingName,
                    _ValueMapperName,
                    _TargetWorkbookExpression,
                    _DimensionCells,
                    _ObsAttributeCells,
                    _PrimaryValueExpr,
                    _ExcludedCells.Where(ec => ec.ConditionExpression != null).Select(ec => new ConditionExpression(ec.ConditionExpression, ec.Source)),
                    _ExcludedCells.Where(ec => ec.CoordExpression != null).Select(ec => new CellReferenceExpression(ec.CoordExpression, ec.Source, ec.IsExpression)),
                    _ExcludedRows,
                    _ExcludedColumns
                )
            {
                IsActive = _IsActive
            };
        }
    }

    internal class DatasetAttributeLookupXmap : ClassMap<DatasetAttributeLookup>
    {
        private string _code;
        private string _coord;

        public DatasetAttributeLookupXmap()
        {
            Map(m => m.Code)
                .ToAttribute("Code", true)
                .Set(x => _code = x)
                .Converter(new StringConverter());

            Map(m => m.Coord)
                .ToAttribute("Coord", true)
                .Set(x => _coord = x)
                .Converter(new StringConverter());
        }

        protected override DatasetAttributeLookup Return()
        {
            return new DatasetAttributeLookup(_code, _coord);
        }
    }

    internal class ExcelDatasetAttributesDescriptorXmap : ClassMap<ExcelDatasetAttributesDescriptor>
    {
        private readonly List<DatasetAttributeLookup> _datasetAttributeLookups;
        private string _Worksheet;

        public ExcelDatasetAttributesDescriptorXmap()
        {
            _datasetAttributeLookups = new List<DatasetAttributeLookup>();

            Map(m => m.Worksheet)
                .ToAttribute("Worksheet", true)
                .Set(x => _Worksheet = x)
                .Converter(new StringConverter());

            MapCollection(m => m.DatasetAttributeLookups)
                .ToElement("Attribute", true)
                .Set(dc => _datasetAttributeLookups.Add(dc))
                .ClassMap(() => new DatasetAttributeLookupXmap());
        }

        protected override ExcelDatasetAttributesDescriptor Return()
        {
            return new ExcelDatasetAttributesDescriptor(_Worksheet, _datasetAttributeLookups);
        }
    }

    internal class ExcelDimAttributesDescriptorXmap : ClassMap<ExcelDimAttributesDescriptor>
    {
        private readonly List<MatchPattern> _Worksheets;

        private CellReferenceExpression _TopLeft;
        private CellReferenceExpression _BottomRight;
        private SdmxArtifactCellReferenceExpression _attributeExpression;

        private readonly List<SdmxArtifactCellReferenceExpression> _dimensionExpressions;
        private readonly List<AxisReferenceExpression> _ExcludedRows;
        private readonly List<AxisReferenceExpression> _ExcludedColumns;
        private readonly List<ExcludedCellRefXMap.ExcludedCell> _ExcludedCells;

        public ExcelDimAttributesDescriptorXmap()
        {
            _Worksheets = new List<MatchPattern>();
            _dimensionExpressions = new List<SdmxArtifactCellReferenceExpression>();

            _ExcludedCells = new List<ExcludedCellRefXMap.ExcludedCell>();
            _ExcludedRows = new List<AxisReferenceExpression>();
            _ExcludedColumns = new List<AxisReferenceExpression>();

            //Map(m => m.Name)
            //    .ToAttribute("Name", true)
            //    .Set(name => _Name = name)
            //    .Converter(new StringConverter());

            //Map(m => m.IsActive)
            //    .ToAttribute("IsActive", false, "true")
            //    .Set(ia => _IsActive = ia)
            //    .Converter(new BooleanConverter());

            MapContainer("Worksheets", true)
                .MapCollection(m => m.Worksheets)
                .ToElement("Worksheet", true)
                .Set(sheet => _Worksheets.Add(sheet))
                .ClassMap(() => new MatchPatternXMap());

            Map(m => m.TopLeft)
                .ToElement("Top-Left", true)
                .Set(cell => _TopLeft = cell)
                .ClassMap(() => new CellRefenceXMap());

            Map(m => m.BottomRight)
                .ToElement("Bottom-Right", true)
                .Set(cell => _BottomRight = cell)
                .ClassMap(() => new CellRefenceXMap());

            Map(m => m.AttributeExpression)
                .ToElement("Attribute", true)
                .Set(x => _attributeExpression = x)
                .ClassMap(() => new SdmxArtifactCellReferenceExpressionXMap(false));

            MapCollection(m => m.DimensionExpressions)
                .ToElement("Dimension", true)
                .Set(dc => _dimensionExpressions.Add(dc))
                .ClassMap(() => new SdmxArtifactCellReferenceExpressionXMap());


            //var mc = MapContainer("Exclude", false);
            //mc.MapCollection(m => m.ExcludedRows)
            //    .ToElement("Row", false)
            //    .Set(row => _ExcludedRows.Add(row))
            //    .ClassMap(() => new AxisRefenceXMap());

            //mc.MapCollection(m => m.ExcludedColumns)
            //    .ToElement("Column", false)
            //    .Set(col => _ExcludedColumns.Add(col))
            //    .ClassMap(() => new AxisRefenceXMap());

            //mc.MapCollection
            //    (m => m.ExcludedCells.Select
            //        (ec => new ExcludedCellRefXMap.ExcludedCell
            //        {
            //            CoordExpression = ec.CoordExpression,
            //            IsExpression = ec.IsExpression
            //        })
            //        .Concat
            //        (m.ExcludeCellConditions.Select
            //            (ec => new ExcludedCellRefXMap.ExcludedCell
            //            {
            //                ConditionExpression = ec.Condition
            //            })))
            //    .ToElement("Cell", false)
            //    .Set(cell => _ExcludedCells.Add(cell))
            //    .ClassMap(() => new ExcludedCellRefXMap());
        }

        protected override ExcelDimAttributesDescriptor Return()
        {
            return new ExcelDimAttributesDescriptor(
                _Worksheets,
                _TopLeft,
                _BottomRight,
                _attributeExpression,
                _dimensionExpressions
            );
        }
    }

    internal class ExcelDataDescriptionXMap : RootElementMap<ExcelDataDescription>
    {
        private readonly List<ExcelCoordMappingDescriptor> _coordMappers;
        private readonly List<ExcelDimAttributesDescriptor> _dimAttributesMappers;
        private ExcelDatasetAttributesDescriptor _datasetAttributesDescriptor;

        private string _Dataflow;
        private string _Agency;
        private string _Version;

        private Dataflow _dataflow;
        private IMappingStoreDataAccess _mappingStoreDataAccess;
        private string _dataspace;

        public override XName Name => "Excel-Data-Description";

        public ExcelDataDescriptionXMap(IMappingStoreDataAccess mappingStoreDataAccess, string dataspace)
        {
            _dataspace = dataspace;
            _mappingStoreDataAccess = mappingStoreDataAccess;

            _coordMappers = new List<ExcelCoordMappingDescriptor>();
            _dimAttributesMappers = new List<ExcelDimAttributesDescriptor>();

            Map(m => m.Dataflow.Code)
                .ToAttribute("Dataflow", true)
                .Set(code => _Dataflow = code)
                .Converter(new StringConverter());

            Map(m => m.Dataflow.Base.AgencyId)
                .ToAttribute("Agency", true)
                .Set(agency => _Agency = agency)
                .Converter(new StringConverter());

            Map(m => m.Dataflow.Base.Version)
                .ToAttribute("Version", true)
                .Set(version => _Version = version)
                .Converter(new StringConverter());

            MapCollection(m => m.CoordMappingDescriptors)
                .ToElement("Coord-Mapping", true)
                .Set(x => _coordMappers.Add(x))
                .ClassMap(() => new ExcelCoordMappingDescriptorXMap(GetDataflow()));

            Map(m => m.DatasetAttributesDescriptor)
                .ToElement("Dataset-Attribute-Mapping", false)
                .Set(x => _datasetAttributesDescriptor = x)
                .ClassMap(() => new ExcelDatasetAttributesDescriptorXmap());

            MapCollection(m => m.DimAttributesDescriptors)
                .ToElement("Dimension-Attribute-Mapping", false)
                .Set(x => _dimAttributesMappers.Add(x))
                .ClassMap(() => new ExcelDimAttributesDescriptorXmap());
        }

        private Dataflow GetDataflow()
        {
            return _dataflow ?? (_dataflow =_mappingStoreDataAccess.GetDataflow(_dataspace, _Agency, _Dataflow, _Version));
        }

        protected override ExcelDataDescription Return()
        {
            return new ExcelDataDescription(0, null, null, GetDataflow(),_coordMappers, _datasetAttributesDescriptor, _dimAttributesMappers);
        }
    }
}