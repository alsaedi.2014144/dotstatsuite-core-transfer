﻿using System.Diagnostics.CodeAnalysis;
using DotStat.Domain;

namespace DotStat.Transfer.Excel.Exceptions
{
    [ExcludeFromCodeCoverage]
    public class IOException : SWApplicationException
    {
        public IOException()
        {
        }

        public IOException(string message) : base(message)
        {
        }

        public IOException(string message, System.Exception inner) : base(message, inner)
        {
        }
    }

    [ExcludeFromCodeCoverage]
    public class DataFormatException : SWApplicationException
    {
        public DataFormatException() : base()
        {
        }

        public DataFormatException(string message) : base(message)
        {
        }

        public DataFormatException(string message, System.Exception inner) : base(message, inner)
        {
        }
    }

    [ExcludeFromCodeCoverage]
    public class ReadErrorException : IOException
    {
        public ReadErrorException() : base()
        {
        }

        public ReadErrorException(string message) : base(message)
        {
        }

        public ReadErrorException(string message, System.Exception inner) : base(message, inner)
        {
        }
    }

    [ExcludeFromCodeCoverage]
    public class MappingException : IOException
    {
        public MappingException()
        {
        }

        public MappingException(string message) : base(message)
        {
        }

        public MappingException(string message, System.Exception inner) : base(message, inner)
        {
        }
    }

    [ExcludeFromCodeCoverage]
    public class MappingNotDefinedException : MappingException
    {
        public readonly Dimension Dimension;
        public readonly Code DimensionMember;
        public readonly string ExternDimension;
        public readonly string ExternDimensionMember;

        public MappingNotDefinedException(Dimension dimension = null, Code dimensionMember = null,
            string externDimension = null, string externDimensionMember = null)
        {
            Dimension = dimension;
            DimensionMember = dimensionMember;
            ExternDimension = externDimension;
            ExternDimensionMember = externDimensionMember;
        }
    }
}