﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace DotStat.Transfer.Excel.Exceptions
{
    [ExcludeFromCodeCoverage]
    public class ApplicationArgumentException : ApplicationException
    {
        public ApplicationArgumentException() : base()
        {
        }

        private static string ExtractActualMessage(string message)
        {
            if (message == null)
            {
                return null;
            }
            var idx = message.IndexOf(":-", StringComparison.Ordinal);
            if (idx < 0)
            {
                return message;
            }
            return message.Substring(idx + 2);
        }

        public ApplicationArgumentException(string message) : base(ExtractActualMessage(message))
        {
        }

        public ApplicationArgumentException(string message, System.Exception inner)
            : base(ExtractActualMessage(message), inner)
        {
        }
    }
}