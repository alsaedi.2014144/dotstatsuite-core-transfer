﻿using System;
using System.Collections.Generic;
using SC = System.Collections;

namespace DotStat.Transfer.Excel.Util
{
    public class Indexer<T> : IIndexer<T>
    {
        public static readonly IIndexer<T> Empty = new Indexer<T>(new T[] {});
        protected static readonly IEnumerator<T> EmptyEnum = Empty.GetEnumerator();
        private readonly object _SyncRoot;
        private IList<T> _Elements;
        private bool _IsSynchronized;

        public Indexer(IEnumerable<T> coll, bool isReadonlyCollection)
        {
            _SyncRoot = new object();
            ReLoad(coll, isReadonlyCollection);
        }

        public Indexer(IEnumerable<T> coll) : this(coll, false)
        {
        }

        public Indexer(IEnumerator<T> coll)
        {
            _SyncRoot = new object();
            FillNewList(coll);
            _IsSynchronized = true;
        }

        protected IList<T> Elements
        {
            get { return _Elements; }
        }

        #region IIndexer<T> Members

        public T this[int idx]
        {
            get { return _Elements[idx]; }
        }

        public virtual bool Contains(T element)
        {
            return _Elements.Contains(element);
        }

        public virtual int IndexOf(T element)
        {
            return _Elements.IndexOf(element);
        }

        void SC.ICollection.CopyTo(Array array, int index)
        {
        }

        public int Count
        {
            get { return _Elements.Count; }
        }

        bool SC.ICollection.IsSynchronized
        {
            get { return _IsSynchronized; }
        }

        object SC.ICollection.SyncRoot
        {
            get { return _SyncRoot; }
        }

        SC.IEnumerator SC.IEnumerable.GetEnumerator()
        {
            return _Elements.GetEnumerator();
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return _Elements.GetEnumerator();
        }

        #endregion

        public virtual void ReLoad(IEnumerable<T> coll)
        {
            ReLoad(coll, false);
        }

        protected virtual void ReLoad(IEnumerable<T> coll, bool isReadOnlyCollection)
        {
            if (this == Empty)
            {
                throw new InvalidProgramException("Empty indexer cannot be loaded");
            }
            var oth = coll as Indexer<T>;
            if (oth != null)
            {
                _Elements = oth._Elements;
                _IsSynchronized = oth._IsSynchronized || isReadOnlyCollection;
                return;
            }

            IList<T> list = coll as List<T> ?? (IList<T>)(coll as T[]);
            if (list != null)
            {
                _Elements = list;
                _IsSynchronized = isReadOnlyCollection;
                var el = coll as List<T>;
                if (el != null)
                {
                    el.Capacity = el.Count;
                }
                return;
            }
            _Elements = FillNewList(coll.GetEnumerator());
            _IsSynchronized = true;
        }

        private static IList<T> FillNewList(IEnumerator<T> el)
        {
            var lst = new List<T>();
            while (el.MoveNext())
            {
                lst.Add(el.Current);
            }
            lst.Capacity = lst.Count;
            return lst;
        }

        #region Nested type: DescendingSortedListEnumerator

        protected class DescendingSortedListEnumerator : IEnumerator<T>
        {
            private readonly IList<T> _idx;
            private int _pos;

            public DescendingSortedListEnumerator(IList<T> idx)
            {
                _idx = idx;
                _pos = idx.Count;
            }

            #region IEnumerator<T> Members

            public T Current
            {
                get
                {
                    if (_pos >= _idx.Count)
                    {
                        throw new InvalidOperationException("Iteration not yet started");
                    }
                    return _idx[_pos];
                }
            }

            public void Dispose()
            {
            }

            object SC.IEnumerator.Current
            {
                get { return Current; }
            }

            public bool MoveNext()
            {
                _pos--;
                return _pos >= 0;
            }

            public void Reset()
            {
                _pos = _idx.Count;
            }

            #endregion
        }

        #endregion
    }
}