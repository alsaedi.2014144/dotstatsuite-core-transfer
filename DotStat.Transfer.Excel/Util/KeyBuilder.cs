﻿namespace DotStat.Transfer.Excel.Util
{

    public static class KeyBuilder
    {
        public static string BuildKey(params int[] src)
        {
            //return string.Join( ";", Array.ConvertAll<int, string>( src, elem => elem.ToString()  ) );
            string res;
            unsafe
            {
                fixed (int* fxsrc = src)
                {
                    res = new string((char*)fxsrc, 0, src.Length*2);
                }
            }
            return res;
        }

        public static int[] GetArray(string key)
        {
            int length = key.Length/2;
            int[] res;
            if (key.Length%2 != 0)
            {
                res = new int[length + 1];
            }
            else
            {
                res = new int[length];
            }
            unsafe
            {
                fixed (char* fxkey = key)
                {
                    fixed (int* fxres = res)
                    {
                        var kp = (int*)fxkey;
                        int* rp = fxres;
                        for (int ii = 0; ii < length; ii++)
                        {
                            *rp = *kp;
                            rp++;
                            kp++;
                        }
                    }
                }
            }
            if (key.Length%2 != 0)
            {
                res[length] = key[key.Length - 1];
            }

            return res;
        }
    }
}