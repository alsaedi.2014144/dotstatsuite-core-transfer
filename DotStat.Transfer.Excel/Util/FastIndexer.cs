﻿using System;
using System.Collections.Generic;

namespace DotStat.Transfer.Excel.Util
{
    public class FastIndexer<T> : Indexer<T>, IFastIndexer<T>
    {
        private static readonly T[] EmptyElems = {};
        public new static readonly IFastIndexer<T> Empty = new FastIndexer<T>(EmptyElems);

        private readonly Dictionary<string, KeyValuePair<Func<T, IComparable>, SortedList<IComparable, T>>> _Indices =
            new Dictionary<string, KeyValuePair<Func<T, IComparable>, SortedList<IComparable, T>>>();

        private Dictionary<T, int> _RankIndex;

        public FastIndexer() : base(new List<T>(0))
        {
        }

        // ReSharper disable once ParameterTypeCanBeEnumerable.Local
        public FastIndexer(IList<T> list) : base(list)
        {
        }

        #region IFastIndexer<T> Members

        public void AddIndex(string idxName, Func<T, IComparable> keyGetter)
        {
            if (_Indices.ContainsKey(idxName))
            {
                throw new ArgumentException("Index name already in use");
            }
            var pair = new KeyValuePair<Func<T, IComparable>, SortedList<IComparable, T>>(keyGetter, null);

            _Indices.Add(idxName, pair);
        }

        public void ResetIndices()
        {
            foreach (var kv in _Indices)
            {
                if (kv.Value.Value != null)
                {
                    kv.Value.Value.Clear();
                    kv.Value.Value.Capacity = 0;
                }
            }

            if (_RankIndex != null)
            {
                _RankIndex.Clear();
                _RankIndex = null;
            }
        }

        public override bool Contains(T element)
        {
            return IndexOf(element) >= 0;
        }

        public override int IndexOf(T item)
        {
            if (Elements.Count == 0)
            {
                return -1;
            }

            if (Equals(Elements[0], item))
            {
                // an optimization for a single element list
                return 0;
            }

            if (_RankIndex == null)
            {
                BuildRankIndex();
            }

            int res;
            // ReSharper disable once PossibleNullReferenceException
            if (_RankIndex.TryGetValue(item, out res))
            {
                return res;
            }
            return -1;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return Elements.GetEnumerator();
        }

        public bool TryGetValue(string idxName, IComparable key, out T res)
        {
            if (Elements.Count == 0)
            {
                res = default(T);
                return false;
            }
            KeyValuePair<Func<T, IComparable>, SortedList<IComparable, T>> idx;

            if (!_Indices.TryGetValue(idxName, out idx))
            {
                throw new ArgumentException("Index not found");
            }
            if (idx.Value == null
                || idx.Value.Count == 0)
            {
                BuildIndex(idxName);
                idx = _Indices[idxName];
            }

            return idx.Value.TryGetValue(key, out res);
        }

        #endregion

        private void BuildIndex(string idxName)
        {
            if (Elements.Count == 0)
            {
                return;
            }

            KeyValuePair<Func<T, IComparable>, SortedList<IComparable, T>> idx = _Indices[idxName];
            idx = new KeyValuePair<Func<T, IComparable>, SortedList<IComparable, T>>(idx.Key,
                new SortedList<IComparable, T>(Elements.Count));
            _Indices[idxName] = idx;

            for (int ii = 0; ii < Elements.Count; ii++)
            {
                T elem = Elements[ii];
                if (!idx.Value.ContainsKey(idx.Key(elem)))
                {
                    idx.Value.Add(idx.Key(elem), elem);
                }
            }
        }

        private void BuildRankIndex()
        {
            _RankIndex = new Dictionary<T, int>(Elements.Count);
            for (int ii = 0; ii < Elements.Count; ii++)
            {
                if (!_RankIndex.ContainsKey(Elements[ii]))
                {
                    _RankIndex.Add(Elements[ii], ii);
                }
            }
        }

        protected override void ReLoad(IEnumerable<T> coll, bool isReadOnlyCollection)
        {
            base.ReLoad(coll, isReadOnlyCollection);
            ResetIndices();
        }

        public override void ReLoad(IEnumerable<T> coll)
        {
            base.ReLoad(coll);
            ResetIndices();
        }

        public virtual void Clear()
        {
            ReLoad(new T[] {});
        }
    }
}