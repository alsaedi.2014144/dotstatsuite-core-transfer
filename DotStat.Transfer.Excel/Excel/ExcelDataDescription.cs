﻿using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using DotStat.Domain;
using DotStat.MappingStore;
using DotStat.Transfer.Excel.Exceptions;
using DotStat.Transfer.Excel.OXM;
using DotStat.Transfer.Excel.Reader;
using DotStat.Transfer.Excel.Util;
using Org.Sdmxsource.Sdmx.Api.Model.Data;

namespace DotStat.Transfer.Excel.Excel
{
    public class ExcelDataDescription
    {
        public static ExcelDataDescription Build(int id, string name, string description, XDocument dataDescription,
            IMappingStoreDataAccess mappingStoreDataAccess, string dataspace)
        {
            try
            {
                var xmap = new ExcelDataDescriptionXMap(mappingStoreDataAccess, dataspace);

                using (var rd = dataDescription.CreateReader())
                {
                    var res = xmap.ReadXml(rd);

                    res.Id = id;
                    res.Name = name;
                    res.Description = description;
                    return res;
                }
            }
            catch (XmlException ex)
            {
                throw new ApplicationArgumentException(
                    "The EDD file {0} could not be loaded\nAn XML error detected: {1}".F(name, ex.Message));
            }
        }

        private readonly List<ExcelCoordMappingDescriptor> _CoordMappingDescriptors;
        private readonly List<ExcelDimAttributesDescriptor> _DimAttributesDescriptors;

        public ExcelDataDescription(
            int id,
            string name,
            string description,
            Dataflow dataflow,
            IEnumerable<ExcelCoordMappingDescriptor> coordinateTranslators,
            ExcelDatasetAttributesDescriptor datasetAttributesDescriptor = null,
            IEnumerable<ExcelDimAttributesDescriptor> dimAttributesDescriptors = null 
            )
        {
            Id = id;
            Name = name ?? string.Empty;
            Description = description ?? string.Empty;
            Dataflow = dataflow;
            
            _CoordMappingDescriptors = new List<ExcelCoordMappingDescriptor>(coordinateTranslators);
            DatasetAttributesDescriptor = datasetAttributesDescriptor;
            _DimAttributesDescriptors = new List<ExcelDimAttributesDescriptor>(dimAttributesDescriptors ?? Enumerable.Empty<ExcelDimAttributesDescriptor>());

            if (_CoordMappingDescriptors.Count == 0)
            {
                throw new ApplicationArgumentException(
                    "The provided collection does not contain any coordinate translators");
            }

            var mult =
                _CoordMappingDescriptors.GroupBy(m => m.Name.ToLowerInvariant())
                    .Where(gr => gr.Count() > 1)
                    .Select(gr => gr.Key)
                    .ToList();
            if (mult.Count > 0)
            {
                throw new ApplicationArgumentException(
                    "There are multiple coordinate mapping descriptions with the following names: {0}\nPlease use unique names"
                        .F(
                            string.Join(",", mult)));
            }

            foreach (var ct in _CoordMappingDescriptors)
            {
                ct.Owner = this;
            }
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public readonly Dataflow Dataflow;

        public IEnumerable<ExcelCoordMappingDescriptor> CoordMappingDescriptors => _CoordMappingDescriptors;

        public ExcelDatasetAttributesDescriptor DatasetAttributesDescriptor { get; private set; }

        public IEnumerable<ExcelDimAttributesDescriptor> DimAttributesDescriptors => _DimAttributesDescriptors;

        public void AddCoordTranslator(ExcelCoordMappingDescriptor ct)
        {
            ct.Owner = this;
            _CoordMappingDescriptors.Add(ct);
        }

        public void AddDimAttributesDescriptor(ExcelDimAttributesDescriptor item)
        {
            _DimAttributesDescriptors.Add(item);
        }

        public IRecordIterator<IObservation> GetObservationCellIterator(IExcelDataSource dataSource)
        {
            var iterators = CoordMappingDescriptors
                .Where(ct => ct.IsActive)
                .SelectMany(ct => ct.BuildIterators(dataSource));

            return new IteratorChain<ExcelCellObservationIterator, IObservation>(iterators);
        }

        public IRecordIterator<IKeyValue> GetDatasetAttributesIterator(IExcelDataSource dataSource)
        {
            return new ExcelCellDatasetAttrIterator(this.DatasetAttributesDescriptor, this.Dataflow, dataSource);
        }

        public IRecordIterator<IKeyable> GetDimAttributesIterator(IExcelDataSource dataSource)
        {
            var iterators = DimAttributesDescriptors
                .SelectMany(ct => ct.BuildIterators(dataSource, Dataflow));

            return new IteratorChain<ExcelCellDimensionAttrIterator, IKeyable>(iterators);
        }
    }
}