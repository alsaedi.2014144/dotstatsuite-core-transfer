using System;
using System.Collections.Generic;
using System.Linq;
using DotStat.Domain;
using DotStat.Transfer.Excel.Exceptions;
using DotStat.Transfer.Excel.Util;

namespace DotStat.Transfer.Excel.Mapping
{
    public class StandardSWMapper : SWMapper
    {
        private readonly HashSet<ExternDimensionSWDimensionPair> _CoordMappings;
        //private SWDataset _ReversedDataset;
        private StandardSWMapper _ReversedMapper;

        public StandardSWMapper(Dataflow targetDataset,
            IEnumerable<string> externColumnNames = null,
            double powerFactor = 1.0,
            int decimalPlaces = 14,
            bool retainTextDataFormatting = true,
            string externDataset = "",
            string externalValueColumn = null,
            string externalControlCodesColumn = null,
            string name = null)
            //, IEnumerable< NameMatching > matchingColumns = null, string[ ] targetTemplate = null )
            : base(targetDataset,
                powerFactor,
                decimalPlaces,
                externDataset,
                externColumnNames,
                null,
                //matchingColumns,
                null,
                //targetTemplate,
                externalValueColumn,
                externalControlCodesColumn)
        {
            RetainTextFormat = retainTextDataFormatting;
            Name = string.IsNullOrWhiteSpace(name) ? string.Empty : name;
            _CoordMappings = new HashSet<ExternDimensionSWDimensionPair>();
            // ReSharper disable once CompareOfFloatsByEqualityOperator
            IsTransforming = retainTextDataFormatting == false || powerFactor != 1.0 || decimalPlaces != 14 ||
                             FixedTargetCount != 0
                             || MatchingNames.Any(mn => !mn.SourceName.IsSameAs(mn.TargetName));
        }

        //internal StandardSWMapper( StandardSWMapper src, double powerFactor = 1.0, int decimalPlaces = 14, bool retainTextDataFormatting = true, bool copyMappings = true )
        //: this(
        //    src.TargetDataset,
        //    externColumnNames: src.SourceNames,
        //    powerFactor: powerFactor,
        //    decimalPlaces: decimalPlaces,
        //    retainTextDataFormatting: retainTextDataFormatting,
        //    externDataset: src.SourceDataset,
        //    externalValueColumn: src.ValueFieldName,
        //    externalControlCodesColumn: src.CCFieldName,
        //    name: src.Name ) {
        //    if ( !copyMappings ) {
        //        return;
        //    }

        //    AdoptMappingsFrom( src );
        //}

        public void AdoptMappingsFrom(StandardSWMapper mapper)
        {
            foreach (var cm in mapper.CoordMappings)
            {
                AddCoordMapping(cm);
            }

            foreach (var cm in mapper.ValueMappings)
            {
                AddValueMapping(cm.Projection[0], cm.Projection[1], cm.KeySource[0], cm.KeySource[1], cm.IsRegEx);
            }

            foreach (var cm in mapper.AdvancedCoordMappings)
            {
                base.AddMapping(cm.KeySource, cm.Projection, cm.IsRegEx);
            }
        }

        public IEnumerable<ExternDimensionSWDimensionPair> CoordMappings
        {
            get { return _CoordMappings; }
        }


        /// <summary>
        /// Add a mapping for a dimension pair where a one-to-one mapping is established
        /// </summary>
        public void AddCoordMapping(ExternDimensionSWDimensionPair pair, bool throwOnError = true)
        {
            if (_CoordMappings.Contains(pair))
            {
                if (throwOnError)
                {
                    throw new ApplicationArgumentException(
                        "The mapping for the pair {0} -> {1} has already been defined".F(pair.ExternDimension,
                            pair.Dimension));
                }
                return;
            }

            var targetIdx = TargetNames.IndexOf(pair.Dimension);

            if (targetIdx < 0
                && pair.Dimension != string.Empty
                && pair.FixedExternMember == null
                && pair.FixedMember == null)
            {
                if (throwOnError)
                {
                    throw new ApplicationArgumentException(
                        "The dimension {0} is not defined in the list of target dimensions".F(pair.Dimension));
                }
                return;
            }

            if (pair.FixedMember != null)
            {
                var idx = TargetNames.IndexOf(pair.Dimension, StringComparer.InvariantCultureIgnoreCase);

                if (idx >= 0)
                {
                    // idx might be < 0 if that is from a reverse mapper
                    var fm = TargetTemplate[idx];

                    if (fm != null
                        && !string.Equals(fm, pair.FixedMember))
                    {
                        if (throwOnError)
                        {
                            throw new ApplicationArgumentException(
                                "A fixed member for the dimension {0} has already been specified.".F(pair.Dimension));
                        }
                        return;
                    }

                    UpdateTargetTemplate(targetIdx, pair.FixedMember);
                }
            }

            else if (pair.FixedExternMember != null)
            {
                // noop here
            }
            else
            {
                if (pair.ExternDimension != null)
                {
                    if (pair.ExternDimension != string.Empty && pair.Dimension != string.Empty)
                    {
                        var srcIdx = SourceNames.IndexOf(pair.ExternDimension, StringComparer.InvariantCultureIgnoreCase);

                        if (srcIdx < 0)
                        {
                            if (throwOnError)
                            {
                                throw new ApplicationArgumentException(
                                    "The dimension {0} is not defined in the list of source dimensions".F(
                                        pair.ExternDimension));
                            }
                            return;
                        }

                        AddNameMatching(pair.ExternDimension, pair.Dimension);

                        foreach (var mi in pair.Mappings)
                        {
                            var src = new string[SourceCount];
                            var target = new string[TargetCount];
                            src[srcIdx] = mi.ExternMember;
                            target[targetIdx] = mi.SWMember;
                            AddMapping(src, target, mi.IsRegex);
                        }
                    }
                    else
                    {
                        AddNameUnMatching(pair.Dimension + pair.ExternDimension);
                    }
                }
                else if (SourceNames.Contains(pair.Dimension) && TargetNames.Contains(pair.Dimension))
                {
                    AddNameUnMatching(pair.Dimension);
                }
                else
                {
                    if (throwOnError)
                    {
                        throw new ApplicationArgumentException(
                            "The un-matching dimension {0} is not defined in the list of source or target dimensions".F(
                                pair.Dimension));
                    }
                }
            }
            IsTransforming = true;
            _CoordMappings.Add(pair);
        }

        /// <summary>
        ///     The mapper will treat the given extern value as null when converting to statworks
        /// </summary>
        public void AddValueMapping(string targetValue, string targetCC, string sourceValue, string sourceCC,
            bool isRegex = false)
        {
            throw new NotImplementedException();

            //var src = new string[ SourceCount ];
            //var target = new string[ TargetCount ];
            //src[ SourceValueIndex ] = sourceValue;

            //if ( SourceCCIndex >= 0 ) {
            //    src[ SourceCCIndex ] = sourceCC;
            //}

            //target[ TargetValueIndex ] = targetValue;

            //if ( TargetCCIndex >= 0 ) {
            //    target[ TargetCCIndex ] = targetCC;
            //}

            //AddMapping( src, target, isRegex );
        }

        public override void AddMapping(string[] source, string[] target, bool isRegex)
        {
            base.AddMapping(source, target, isRegex);
            IsTransforming = true;
        }

        public override void UpdatePowerFactor(double pf)
        {
            base.UpdatePowerFactor(pf);

            // ReSharper disable once CompareOfFloatsByEqualityOperator
            if (PowerFactor != 1.0)
            {
                IsTransforming = true;
            }
        }

        public override bool Equals(object obj)
        {
            var oth = obj as StandardSWMapper;

            if (oth == null
                || !base.Equals(oth))
            {
                return false;
            }

            var cm = new HashSet<ExternDimensionSWDimensionPair>(CoordMappings);

            if (!cm.SetEquals(oth.CoordMappings))
            {
                return false;
            }

            return true;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return base.GetHashCode();
            }
        }


        /// <summary>
        /// this method assumes newTargetDataset parameter is fully name compatible with the current mapper
        /// </summary>
        /// <param name="newTargetDataset"></param>
        /// <returns></returns>
        public StandardSWMapper GetReverseMapper(Dataflow newTargetDataset)
        {
            if (_ReversedMapper != null)
            {
                return _ReversedMapper;
            }
            // ReSharper disable once CompareOfFloatsByEqualityOperator
            var res = new StandardSWMapper(newTargetDataset,
                externColumnNames: TargetNames,
                powerFactor: PowerFactor != 1.0 ? 1.0/PowerFactor : 1.0,
                decimalPlaces: DecimalPlaces,
                retainTextDataFormatting: RetainTextFormat,
                externDataset: TargetDataset.Code,
                externalValueColumn: TargetDataset.ValueFieldName,
                externalControlCodesColumn: null);
            AddReversedMappings(res);
            res._ReversedMapper = this;
            _ReversedMapper = res;
            return res;
        }

        private void AddReversedMappings(StandardSWMapper mapper)
        {
            foreach (var pair in CoordMappings)
            {
                var rp = new ExternDimensionSWDimensionPair(mapper.TargetDataset.FindDimension(pair.ExternDimension),
                    pair.ExternDimension,
                    pair.Dimension,
                    pair.FixedExternMember,
                    pair.FixedMember);

                foreach (var mi in pair.Mappings.Where(m => !m.IsRegex))
                {
                    rp.AddMapping(mi.SWMember, mi.ExternMember);
                }

                mapper.AddCoordMapping(rp);
            }

            foreach (var vm in ValueMappings.Where(v => !v.IsRegEx))
            {
                mapper.AddValueMapping(vm.KeySource[0], vm.KeySource[1], vm.Projection[0], vm.Projection[1]);
            }

            foreach (var mi in AdvancedCoordMappings.Where(m => !m.IsRegEx))
            {
                mapper.AddMapping(mi.Projection, mi.KeySource, false);
            }
        }

        public StandardSWMapper BuildReverseValueMapper()
        {
            var valueMapper = new StandardSWMapper(TargetDataset,
                TargetNames,
                1.0/PowerFactor,
                DecimalPlaces,
                RetainTextFormat,
                TargetDataset.Code,
                TargetDataset.ValueFieldName,
                null,
                "Reverse-Value-Mapper: " + Name);

            foreach (var vm in ValueMappings.Where(m => !m.IsRegEx))
            {
                valueMapper.AddValueMapping(vm.KeySource[0], vm.KeySource[1], vm.Projection[0], vm.Projection[1]);
            }

            return valueMapper;
        }
    }
}