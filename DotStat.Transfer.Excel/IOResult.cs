﻿using System;
using System.Diagnostics.CodeAnalysis;
using Org.Sdmxsource.Sdmx.Api.Model.Data;

namespace DotStat.Transfer.Excel
{

    [ExcludeFromCodeCoverage]
    public class IOResult
    {
        public static readonly IOResult OK = new IOResult(IOStatus.OK);
        public static readonly IOResult EOF = new IOResult(IOStatus.EOF);

        public readonly string StatusMessage;
        public readonly IOStatus Status;
        public readonly IObservation Observation;

        /// <summary>
        /// Any error occured during processing this particular observation
        /// </summary>
        public readonly System.Exception Error;

        /// <summary>
        /// Source datga, in case there is mapping involved to reach to the final data
        /// </summary>
        public readonly string[] SourceData;

        public IOResult(IOStatus status,
            string statusMessage = "",
            IObservation observation = null,
            System.Exception error = null,
            string[] sourceData = null)
        {
            StatusMessage = statusMessage ?? String.Empty;
            Observation = observation;
            Status = status;
            Error = error;
            SourceData = sourceData;
        }
    }
}