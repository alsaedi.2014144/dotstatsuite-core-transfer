﻿using DotStat.Transfer.Consumer;
using DotStat.Transfer.Param;
using DotStat.Transfer.Producer;

namespace DotStat.Transfer.Manager
{
    public interface ITransferManager<T> where T : TransferParam
    {
        void Transfer(T transferParam);
        IProducer<T> Producer { get; set; }
        IConsumer<T> Consumer { get; set; }
    }
}