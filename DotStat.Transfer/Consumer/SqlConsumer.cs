﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using DotStat.Common.Auth;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Localization;
using DotStat.Common.Logger;
using DotStat.Db.Exception;
using DotStat.Db.Repository;
using DotStat.Db.Repository.SqlServer;
using DotStat.Db.Util;
using DotStat.Db.Validation;
using DotStat.Db.Validation.SqlServer;
using DotStat.Domain;
using DotStat.MappingStore;
using DotStat.Transfer.DataflowManager;
using DotStat.Transfer.Exception;
using DotStat.Transfer.Param;

namespace DotStat.Transfer.Consumer
{
    public class SqlConsumer : IConsumer<ITransferParam>, IDataflowManager<ISqlTransferParam>
    {
        private readonly IMappingStoreDataAccess _mappingStoreDataAccess;
        private readonly IAuthorizationManagement _authorizationManagement;
        private readonly IGeneralConfiguration _generalConfiguration;

        public SqlConsumer(IMappingStoreDataAccess mappingStoreDataAccess, IAuthorizationManagement authorizationManagement, IGeneralConfiguration generalConfiguration)
        {
            _mappingStoreDataAccess = mappingStoreDataAccess;
            _authorizationManagement = authorizationManagement;
            _generalConfiguration = generalConfiguration;
        }

        public Dataflow GetDataflow(ISqlTransferParam transferParam)
        {
            var dataflow = _mappingStoreDataAccess.GetDataflow(
                transferParam.DestinationDataspace.Id,
                transferParam.DestinationDataflow.AgencyId,
                transferParam.DestinationDataflow.Id,
                transferParam.DestinationDataflow.Version);

            //Set log dataspaceId and TransactionId?
            Log.Notice( string.Format(
                    LocalizationRepository.GetLocalisedResource(
                        Localization.ResourceId.DataflowLoaded,
                        transferParam.CultureInfo.TwoLetterISOLanguageName),
                    dataflow.FullId,
                    transferParam.DestinationDataspace.Id));

            return dataflow;
        }
        
        [ObsoleteAttribute("This property is obsolete. Use other Save method instead.")]
        [ExcludeFromCodeCoverage]
        public bool Save(ITransferParam transferParam, Dataflow dataflow, TransferContent transferContent)
        {
            var mgmtRepository = new SqlManagementRepository(transferParam.DestinationDataspace, _generalConfiguration);
            var transRepository = new SqlTransactionRepository(transferParam.DestinationDataspace, _generalConfiguration);
            var dataStoreRepository = new SqlDataStoreRepository(transferParam.DestinationDataspace, _generalConfiguration);

            var codeTranslator = new CodeTranslator(mgmtRepository);

            var dotStatDb = mgmtRepository.GetDb(); // Gets the default datastore db

            var datasetAttributeDatabaseValidator = new SqlDatasetAttributeDatabaseValidator(mgmtRepository, dotStatDb, _generalConfiguration);

            var keyableAttributeDatabaseValidator = new SqlKeyableDatabaseValidator(mgmtRepository, dotStatDb, _generalConfiguration);

            var datasetAttributeValidator = new DatasetAttributeValidator(_generalConfiguration);

            return Save(transferParam, dataflow, transferContent, mgmtRepository, transRepository, dataStoreRepository, datasetAttributeDatabaseValidator, keyableAttributeDatabaseValidator, datasetAttributeValidator, codeTranslator);
        }

        public bool Save(
            ITransferParam transferParam, 
            Dataflow dataflow, 
            TransferContent transferContent, 
            IManagementRepository mgmtRepository,
            ITransactionRepository transRepository,
            IDataStoreRepository dataStoreRepository,
            ISqlDatasetAttributeDatabaseValidator datasetAttributeDatabaseValidator,
            ISqlKeyableDatabaseValidator keyableAttributeDatabaseValidator,
            IDatasetAttributeValidator datasetAttributeValidator,
            ICodeTranslator codeTranslator)
        {
            if (!IsAuthorized(transferParam, dataflow))
            {
                throw new TransferUnauthorizedException();
            }

            //Check if Repository objects are instantiated, use fluent validator 
            if (mgmtRepository == null)
            {
                throw new ArgumentNullException(nameof(mgmtRepository));
            }

            if (transRepository == null)
            {
                throw new ArgumentNullException(nameof(transRepository));
            }

            if (dataStoreRepository == null)
            {
                throw new ArgumentNullException(nameof(dataStoreRepository));
            }

            if (datasetAttributeDatabaseValidator == null)
            {
                throw new ArgumentNullException(nameof(datasetAttributeDatabaseValidator));
            }

            if (keyableAttributeDatabaseValidator == null)
            {
                throw new ArgumentNullException(nameof(keyableAttributeDatabaseValidator));
            }

            if (datasetAttributeValidator == null)
            {
                throw new ArgumentNullException(nameof(datasetAttributeValidator));
            }

            if (codeTranslator == null)
            {
                throw new ArgumentNullException(nameof(codeTranslator));
            }

            mgmtRepository.CheckManagementTables(transferParam.Id);

            try
            {
                //In case there is already a PIT release defined on the DSD, 
                //all transaction on this DSD have to target PIT release irrespectively to the value of this parameter.
                if (transferParam.TargetVersion == TargetVersion.Live && mgmtRepository.GetActualDsdPITVersion(dataflow.Dsd) == null)
                    transferParam.TargetVersion = TargetVersion.Live;
                else
                    transferParam.TargetVersion = TargetVersion.PointInTime;

                //check transaction and create one if none exists for the dsd of dataflow.
                //also creates tables/rows for missing artefacts and new data version, populate dataflow and children with database id-s 
                if (!transRepository.TryNewTransaction(transferParam.Id, dataflow, transferParam.TargetVersion, false))
                {
                    transRepository.CleanUpFailedTransaction(transferParam.Id);
                    return false;
                }

                //The dataflow gets populated in TryNewTransaction, update codeTranslator codelists
                codeTranslator.FillDict(dataflow);

                dataflow.Dsd.LiveVersion = (char?)mgmtRepository.GetDsdLiveVersion(dataflow.Dsd);
                dataflow.Dsd.PITVersion = (char?)mgmtRepository.GetDsdPITVersion(dataflow.Dsd);
                dataflow.Dsd.PITReleaseDate = mgmtRepository.GetDsdPITReleaseDate(dataflow.Dsd);

                //Execute PIT Release if there is one pending 
                if (dataflow.Dsd.PITVersion != null //Is there a PIT?
                    && (dataflow.Dsd.PITReleaseDate != null && dataflow.Dsd.PITReleaseDate <= DateTime.Now)) //PIT release date has passed
                {
                    var previousPITReleaseDate = dataflow.Dsd.PITReleaseDate;

                    transRepository.ApplyPITRelease(dataflow, transferParam.PITRestorationAllowed, transferParam.Id, _mappingStoreDataAccess);

                    if (!dataflow.Dsd.LiveVersion.HasValue)
                    {
                        throw new ArgumentNullException(nameof(dataflow.Dsd.LiveVersion), "No value set for dsd.LiveVersion during application of PIT release.");
                    }

                    var newDbTargetVersion = transferParam.TargetVersion == TargetVersion.Live
                        ? (DbTableVersion)dataflow.Dsd.LiveVersion
                        : DbTableVersions.GetNewTableVersion((DbTableVersion)dataflow.Dsd.LiveVersion);

                    mgmtRepository.UpdateTargetVersionOfTransaction(transferParam.Id, newDbTargetVersion);

                    Log.Debug($"DB Target version of transaction targeting [{transferParam.TargetVersion}] release changed to [{newDbTargetVersion}] after applying previous (soft) PIT release active since {previousPITReleaseDate.ToString()}.");
                }

                if (transferParam.TargetVersion != TargetVersion.Live)
                {
                    dataflow.Dsd.PITReleaseDate = transferParam.PITReleaseDate;
                }

                DbTableVersion targetTableVersion;

                //Copy data to PIT version, for the first pit load.
                if (transferParam.TargetVersion == TargetVersion.PointInTime && dataflow.Dsd.PITVersion == null)
                {
                    //Assign new table version to PITVersion
                    dataflow.Dsd.PITVersion = dataflow.Dsd.LiveVersion == null ?
                        (char)DbTableVersion.A : (char)DbTableVersions.GetNewTableVersion((DbTableVersion)dataflow.Dsd.LiveVersion);
                    targetTableVersion = (DbTableVersion)dataflow.Dsd.PITVersion;

                    if (transferParam.PITRestorationAllowed)
                        dataflow.Dsd.PITRestorationDate = DateTime.Now;
                    //Cleanup target PIT tables
                    dataStoreRepository.DeleteData(dataflow.Dsd, targetTableVersion);
                    //Copy data to new data version
                    dataStoreRepository.CopyDataToNewVersion(dataflow.Dsd, targetTableVersion);
                    //Copy attributes to new data version
                    dataStoreRepository.CopyAttributesToNewVersion(dataflow.Dsd, targetTableVersion);
                }
                else if (transferParam.TargetVersion == TargetVersion.PointInTime && dataflow.Dsd.PITVersion != null)
                {
                    targetTableVersion = (DbTableVersion)dataflow.Dsd.PITVersion;
                    dataflow.Dsd.PITRestorationDate = mgmtRepository.GetDsdPITRestorationDate(dataflow.Dsd);
                }
                else
                {
                    //First data load to live version
                    if (dataflow.Dsd.LiveVersion == null)
                        //Assign new table version to liveVersion
                        dataflow.Dsd.LiveVersion = dataflow.Dsd.PITVersion == null ?
                        (char)DbTableVersion.A : (char)DbTableVersions.GetNewTableVersion((DbTableVersion)dataflow.Dsd.PITVersion);

                    targetTableVersion = (DbTableVersion)dataflow.Dsd.LiveVersion;
                }

                // Wipe and/or create staging table and target tables
                dataStoreRepository.RecreateStagingTables(dataflow.Dsd);

                var maxErrorCount = _generalConfiguration.MaxTransferErrorAmount;

                // Load observation data and attributes into staging table
                var attributesReportedAtObservation =
                    dataStoreRepository.BulkInsertData(transferContent.Observations, codeTranslator as CodeTranslator, dataflow, out var errorList);
                
                if (errorList.Count < maxErrorCount || maxErrorCount == 0)
                {
                    var attributeValidator = new KeyableValidator(_generalConfiguration);

                    var bulkCopyCompletedSuccessfully = false;

                    if (attributeValidator.Validate(transferContent.Keyables, attributesReportedAtObservation?.Keyables,
                            dataflow) && errorList.Count == 0) 
                    {
                        // Load dim/group attributes into staging table
                        dataStoreRepository.BulkInsertAttributes(transferContent.Keyables,
                            attributesReportedAtObservation?.Keyables,
                            codeTranslator as CodeTranslator, dataflow);

                        bulkCopyCompletedSuccessfully = true;
                    }

                    errorList.AddRange(attributeValidator.GetErrors());

                    if (errorList.Count < maxErrorCount || maxErrorCount == 0)
                    {
                        var datasetAttributeKeyValues = transferContent.DatasetAttributes?.ToList();

                        datasetAttributeValidator.Validate(datasetAttributeKeyValues,
                            attributesReportedAtObservation?.DatasetAttributes, dataflow,
                            maxErrorCount > 0 ? maxErrorCount - errorList.Count : 0);

                        errorList.AddRange(datasetAttributeValidator.GetErrors());

                        if (errorList.Count < maxErrorCount || maxErrorCount == 0)
                        {
                            datasetAttributeDatabaseValidator.Validate(targetTableVersion, datasetAttributeKeyValues,
                                attributesReportedAtObservation?.DatasetAttributes, dataflow,
                                maxErrorCount > 0 ? maxErrorCount - errorList.Count : 0);

                            errorList.AddRange(datasetAttributeDatabaseValidator.GetErrors());

                            if (bulkCopyCompletedSuccessfully && errorList.Count < maxErrorCount || maxErrorCount == 0) // No validation when no data in staging tables
                            {
                                keyableAttributeDatabaseValidator.Validate(targetTableVersion, dataflow, transferParam.DestinationDataspace, codeTranslator, maxErrorCount > 0 ? maxErrorCount - errorList.Count : 0);

                                errorList.AddRange(keyableAttributeDatabaseValidator.GetErrors());
                            }
                        }
                    }
                }

                if (errorList.Count > 0)
                {
                    throw new ConsumerValidationException(errorList);
                }

                //merge data 
                var observationsProcessed = dataStoreRepository.MergeStagingToFactData(dataflow.Dsd, targetTableVersion);

                dataStoreRepository.MergeStagingToPivotDimGroupAttributes(dataflow.Dsd);

                dataStoreRepository.MergePivotToDimGroupAttributes(dataflow.Dsd, targetTableVersion);

                dataStoreRepository.MergeStagingToDatasetAttributes(dataflow, transferContent.DatasetAttributes,
                    attributesReportedAtObservation?.DatasetAttributes, codeTranslator as CodeTranslator, targetTableVersion);

                //set transaction ready for validation
                mgmtRepository.MarkTransactionReadyForValidation(transferParam.Id);

                //Clean restoration
                if (transferParam.TargetVersion == TargetVersion.Live)
                {
                    dataStoreRepository.DeleteData(dataflow.Dsd,
                        DbTableVersions.GetNewTableVersion((DbTableVersion)dataflow.Dsd.LiveVersion));
                }

                Log.Notice(string.Format(
                    LocalizationRepository.GetLocalisedResource(
                        Localization.ResourceId.ObservationsProcessed,
                        transferParam.CultureInfo.TwoLetterISOLanguageName),
                    observationsProcessed));

                //close the transaction - planned to be done by watchdog
                transRepository.CloseTransaction(transferParam.Id, dataflow, _mappingStoreDataAccess, targetTableVersion, transferParam.PITRestorationAllowed);
                
            }
            catch (System.Exception)
            {
                transRepository.CleanUpFailedTransaction(transferParam.Id);
                throw;
            }

            return true;
        }

        

        public bool IsAuthorized(ITransferParam transferParam, Dataflow dataflow)
        {
            return _authorizationManagement.IsAuthorized(
                transferParam.Principal,
                transferParam.DestinationDataspace.Id,
                dataflow.AgencyId,
                dataflow.Base.Id,
                dataflow.Version.ToString(),
                PermissionType.CanImportData
            );
        }
    }
}