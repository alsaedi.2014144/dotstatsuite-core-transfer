﻿using DotStat.Domain;
using DotStat.MappingStore;
using DotStat.Transfer.Param;
using Org.Sdmxsource.Sdmx.Api.Engine;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using Org.Sdmxsource.Sdmx.DataParser.Engine.Csv;
using Org.Sdmxsource.Sdmx.DataParser.Manager;
using Org.Sdmxsource.Util.Io;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;

namespace DotStat.Transfer.Producer
{
    public class SdmxFileProducer : IProducer<SdmxFileToSqlTransferParam>
    {
        private readonly ReadableDataLocationFactory _dataLocationFactory;
        private readonly IMappingStoreDataAccess _dataAccess;

        public SdmxFileProducer(IMappingStoreDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
            _dataLocationFactory = new ReadableDataLocationFactory();
        }

        public bool IsAuthorized(SdmxFileToSqlTransferParam transferParam, Dataflow dataflow)
        {
            return true;
        }

        public Dataflow GetDataflow(SdmxFileToSqlTransferParam transferParam)
        {
            return _dataAccess.GetDataflow(transferParam.DestinationDataspace.Id,
                transferParam.SourceDataflow.AgencyId,
                transferParam.SourceDataflow.Id,
                transferParam.SourceDataflow.Version
            );
        }

        public TransferContent Process(SdmxFileToSqlTransferParam transferParam, Dataflow dataflow)
        {
            var fileInfo = new FileInfo(transferParam.FilePath);

            if (!fileInfo.Exists)
                throw new ArgumentException($"File [{transferParam.FilePath}] not found");

            var extension = (transferParam.Extension ?? fileInfo.Extension).ToLower();

            if (extension == ".zip")
            {
                var extractedFile = Path.GetTempFileName();

                using (var zip = ZipFile.OpenRead(fileInfo.FullName))
                {
                    var entry = zip.Entries.First();
                    extension = Path.GetExtension(entry.FullName)?.ToLower();
                    entry.ExtractToFile(extractedFile, true);
                    fileInfo = new FileInfo(extractedFile);
                }
            }

            var type = GetInputType(extension);

            return Process(type, fileInfo.FullName, dataflow);
        }

        private TransferContent Process(InputType type, string filename, Dataflow dataflow)
        {
            var keyables = new List<IKeyable>();
            var datasetAttributes = new List<IKeyValue>();
            var content = new TransferContent()
            {
                Keyables = keyables,
                DatasetAttributes = datasetAttributes
            };

            switch (type)
            {
                case InputType.Xml:
                    content.Observations = ProcessSdmxXml(filename, dataflow, keyables, datasetAttributes);
                    break;

                case InputType.Csv:
                    content.Observations = ProcessSdmxCsv(filename, keyables, datasetAttributes);
                    break;
            }

            return content;
        }

        private IEnumerable<IObservation> ProcessSdmxCsv(string filename, List<IKeyable> keyables, List<IKeyValue> datasetAttributes)
        {
            using (var sourceData = _dataLocationFactory.GetReadableDataLocation(new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)))
            {
                using (var dataReaderEngine = new CsvDataReaderEngine(sourceData, _dataAccess.GetRetrievalManager(), null, null))
                {
                    foreach (var observation in ReadSdmxFile(dataReaderEngine, keyables, datasetAttributes))
                        yield return observation;
                }
            }
        }

        private IEnumerable<IObservation> ProcessSdmxXml(string filename, Dataflow dataflow, List<IKeyable> keyables, List<IKeyValue> datasetAttributes)
        {
            var manager = new DataReaderManager();

            using (var sourceData = _dataLocationFactory.GetReadableDataLocation(new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)))
            {
                using (var dataReaderEngine = manager.GetDataReaderEngine(sourceData, dataflow.Dsd.Base, null))
                {
                    foreach (var observation in ReadSdmxFile(dataReaderEngine, keyables, datasetAttributes))
                        yield return observation;
                }
            }
        }

        private IEnumerable<IObservation> ReadSdmxFile(IDataReaderEngine reader, List<IKeyable> keyables, List<IKeyValue> datasetAttributes)
        {
            keyables.Clear();
            datasetAttributes.Clear();

            if (reader.MoveNextDataset())
            {
                datasetAttributes.AddRange(reader.DatasetAttributes);

                while (reader.MoveNextKeyable())
                {
                    var currentKeyable = reader.CurrentKey;

                    keyables.Add(currentKeyable);

                    if (reader.CurrentKey.Series && reader.MoveNextObservation())
                    {
                        do
                        {
                            var currentObservation = reader.CurrentObservation;

                            yield return currentObservation;
                        }
                        while (reader.MoveNextObservation());
                    }
                }
            }
        }

        private InputType GetInputType(string extension)
        {
            switch (extension)
            {
                case ".xml":
                    return InputType.Xml;

                case ".csv":
                    return InputType.Csv;

                case ".json":
                    throw new NotImplementedException("Json reader needs implementation in SdmxSource library");

                default:
                    throw new NotImplementedException($"File extension [{extension}] not implemented");
            }
        }

        public void Dispose()
        {
            //Nothing to do, IDataReaderEngine is within a "using" clause
        }

        private enum InputType
        {
            Xml,
            Csv,
            Json
        }
    }
}