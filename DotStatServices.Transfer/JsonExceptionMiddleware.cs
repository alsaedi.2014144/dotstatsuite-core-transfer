﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using DotStat.Common.Logger;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace DotStatServices.Transfer
{
    public sealed class JsonExceptionMiddleware
    {
        private readonly IHostingEnvironment _env;
        private readonly JsonSerializer _serializer;

        public JsonExceptionMiddleware(IHostingEnvironment env)
        {
            _env = env;
            _serializer = new JsonSerializer {ContractResolver = new CamelCasePropertyNamesContractResolver()};
        }

        public async Task Invoke(HttpContext context)
        {
            context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
            context.Response.ContentType = "application/json";

            var ex = context.Features.Get<IExceptionHandlerFeature>()?.Error;

            if (ex == null)
                return;

            Log.Error(ex);

            var error = BuildError(ex, _env);

            using (var writer = new StreamWriter(context.Response.Body))
            {
                _serializer.Serialize(writer, error);
                await writer.FlushAsync().ConfigureAwait(false);
            }
        }

        private static OperationResult BuildError(Exception ex, IHostingEnvironment env)
        {
            return new OperationResult(false, ex.Message);
        }
    }
}
