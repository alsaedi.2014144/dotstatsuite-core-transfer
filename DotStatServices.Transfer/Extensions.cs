﻿using DotStat.Common.Configuration.Dto;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Localization;
using Microsoft.AspNetCore.Http;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;

namespace DotStatServices.Transfer
{
    internal static class Extensions
    {
        public static readonly HashSet<string> AllowedImportSchemes = new HashSet<string>()
        {
            Uri.UriSchemeHttp,
            Uri.UriSchemeHttps
        };

        public static DataspaceInternal GetSpaceInternal(this string space, IDataspaceConfiguration configuration, bool mandatory = true)
        {
            if (string.IsNullOrEmpty(space) && mandatory)
                throw new ArgumentException("Data space ID not provided");

            var dataspace = configuration
                                .SpacesInternal
                                .FirstOrDefault(x => x.Id.Equals(space, StringComparison.InvariantCultureIgnoreCase));

            if (dataspace == null && mandatory)
                throw new ArgumentException($"No dataspace with ID: [{space}]");

            return dataspace;
        }

        public static DataspaceExternal GetExternalSpace(this string space, IDataspaceConfiguration configuration)
        {
            if (string.IsNullOrEmpty(space))
                throw new ArgumentException("Data space ID not provided");

            var dataspace = configuration
                .SpacesExternal
                .FirstOrDefault(x => x.Id.Equals(space, StringComparison.InvariantCultureIgnoreCase));

            if (dataspace == null)
                throw new ArgumentException($"No dataspace with ID: [{space}]");

            return dataspace;
        }

        public static string GetLocalPath(this IFormFile file, string name, params string[] extensions)
        {
            if(file==null || file.Length == 0)
                throw new ArgumentException($"[{name}] attachment is missing.");

            if (!extensions.Any(ext => file.FileName.EndsWith(ext, StringComparison.OrdinalIgnoreCase)))
                throw new ArgumentException($"[{name}] file [{file.FileName}] is not using expected extension [{string.Join(",", extensions)}]");

            var path = Path.GetTempFileName();

            using (var stream = new FileStream(path, FileMode.Open))
                file.CopyTo(stream);

            return path;
        }

        public static DataflowMutableCore GetDataflow(this string dataflow)
        {
            if(string.IsNullOrEmpty(dataflow))
                throw new ArgumentException("Dataflow is missing");

            var elements = dataflow.Split(new char[] { ':', '(', ')' }, StringSplitOptions.RemoveEmptyEntries);

            if (elements.Length != 3)
                throw new ArgumentException($"The format of the dataflow [{dataflow}] is not a valid. It should be AGENCYID:ID(VERSION)");

            return new DataflowMutableCore()
            {
                AgencyId = elements[0],
                Id = elements[1],
                Version = elements[2]
            };
        }

        public static string FullId(this IDataflowMutableObject df)
        {
            if(df == null)
                throw new ArgumentException(nameof(df));

            return $"{df.AgencyId}:{df.Id}({df.Version})";
        }

        public static DataStructureMutableCore GetDsd(this string dsd)
        {
            if (string.IsNullOrEmpty(dsd))
                throw new ArgumentException("Dsd is missing");

            var elements = dsd.Split(new char[] { ':', '(', ')' }, StringSplitOptions.RemoveEmptyEntries);

            if (elements.Length != 3)
                throw new ArgumentException($"The format of the dsd [{dsd}] is not a valid. It should be AGENCYID:ID(VERSION)");

            return new DataStructureMutableCore()
            {
                AgencyId = elements[0],
                Id = elements[1],
                Version = elements[2]
            };
        }

        public static DateTime? GetPITReleaseDate(this string dateTime)
        {
            if (string.IsNullOrEmpty(dateTime))
                return null;
            string format = "dd-MM-yyyy HH:mm:ss";
            try
            {
                return DateTime.ParseExact(dateTime, format, CultureInfo.InvariantCulture);
            }
            catch(System.FormatException e)
            {
                    throw new ArgumentException($"The format of the PITReleaseDate [{dateTime}] is not valid. It should be {format}");
            }
        }

        public static bool IsValidUrl(string url)
        {
            return Uri.TryCreate(url, UriKind.Absolute, out var uriResult) && AllowedImportSchemes.Contains(uriResult.Scheme);
        }

        public static bool IsAccessible(string externalSdmxSourceUrl)
        {
            var request = WebRequest.Create(externalSdmxSourceUrl);
            //Ideally a request to get only the headers (request.Method = WebRequestMethods.Http.Head;) should be made, but some nsiws might not support it
            //As a workaround the contentLength is set to 0, which will return only the headers.
            request.ContentLength = 0;

            try
            {
                using (var response = request.GetResponse() as HttpWebResponse)
                {
                    return response != null && response.StatusCode == HttpStatusCode.OK;
                }
            }
            catch (Exception)
            {
                throw new ArgumentException(message: string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.SdmxSourceHttpRequestException),
                    externalSdmxSourceUrl));
            }

        }
    }
}
