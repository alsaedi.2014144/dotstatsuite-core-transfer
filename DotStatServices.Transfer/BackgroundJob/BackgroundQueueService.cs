﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;

namespace DotStatServices.Transfer.BackgroundJob
{
    internal class BackgroundQueueService : BackgroundService
    {
        private readonly BackgroundQueue _backgroundQueue;

        public BackgroundQueueService(BackgroundQueue backgroundQueue)
        {
            _backgroundQueue = backgroundQueue;
        }

        protected override async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                if (_backgroundQueue.TaskInQueue == 0 || _backgroundQueue.RunningTasks >= _backgroundQueue.MaxConcurrentCount)
                    await Task.Delay(_backgroundQueue.MillisecondsToWaitBeforePickingUpTask, cancellationToken);
                else
                    await _backgroundQueue.Dequeue(cancellationToken);
            }
        }
    }
}
