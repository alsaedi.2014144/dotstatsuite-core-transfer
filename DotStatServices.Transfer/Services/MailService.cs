﻿using System;
using System.Net;
using System.Net.Mail;
using System.Text;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Localization;
using DotStat.Common.Logger;
using log4net.Core;
using static DotStat.Common.Localization.LocalizationRepository;

namespace DotStatServices.Transfer.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class MailService : IMailService
    {
        private readonly IMailConfiguration _configuration;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        public MailService(IMailConfiguration configuration)
        {
            _configuration = configuration;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="transactionResult"></param>
        /// <param name="dataspace"></param>
        /// <param name="transactionId"></param>
        /// <param name="transactionLogs"></param>
        /// <param name="mailTo"></param>
        /// <param name="languageCode"></param>
        public void SendMail(TransactionResult transactionResult, string languageCode)
        {
            if (string.IsNullOrEmpty(transactionResult.User))
                return;

            try
            {
                var transactionMessages = new StringBuilder();
                var hasWarnings = false;
                foreach (var transactionLog in transactionResult.TransactionLogs)
                {
                    string row;
                    if (transactionLog.Level == Level.Error)
                    {
                        row = $"<tr style=\"color:red;font-weight:bold;\"><td>{transactionLog.TimeStamp}</td><td>{transactionLog.Level}</td><td><pre>{transactionLog.RenderedMessage}</pre></td></tr>";
                    }
                    else if (transactionLog.Level == Level.Warn)
                    {
                        hasWarnings = true;
                        row = $"<tr style=\"color:orange;font-weight:bold;\"><td>{transactionLog.TimeStamp}</td><td>{transactionLog.Level}</td><td><pre>{transactionLog.RenderedMessage}</pre></td></tr>";
                    }
                    else { 
                        row = $"<tr><td>{transactionLog.TimeStamp}</td><td>{transactionLog.Level}</td><td><pre>{transactionLog.RenderedMessage}</pre></td></tr>";
                    }

                    transactionMessages.Append(row);
                }
                
                var smtpClient = new SmtpClient(_configuration.SmtpHost, _configuration.SmtpPort)
                {
                    EnableSsl = _configuration.SmtpEnableSsl
                };

                if (!string.IsNullOrEmpty(_configuration.SmtpUserName) &&
                    !string.IsNullOrEmpty(_configuration.SmtpUserPassword) &&
                    !_configuration.SmtpUserName.Equals("na", StringComparison.InvariantCultureIgnoreCase) &&
                    !_configuration.SmtpUserPassword.Equals("na", StringComparison.InvariantCultureIgnoreCase))
                {
                    smtpClient.Credentials = new NetworkCredential(_configuration.SmtpUserName, _configuration.SmtpUserPassword);
                }


                var emailSummarySourceDataSpace = transactionResult.SourceDataSpaceId != null
                    ? string.Format(GetLocalisedResource(
                        Localization.ResourceId.EmailSummarySourceDataSpace, languageCode), transactionResult.SourceDataSpaceId)
                    : string.Empty;

                var status = string.Empty;
                var subjectStatus = transactionResult.TransactionStatus.ToString();
                switch (transactionResult.TransactionStatus)
                {
                    case TransactionStatus.InProgress:
                        status = $"<span style=\"color:orange;font-weight:bold;\">{TransactionStatus.InProgress}</span>";
                        break;
                    case TransactionStatus.Completed when !hasWarnings:
                        status = $"<span style=\"color:green;font-weight:bold;\">{TransactionStatus.Completed}</span>";
                        subjectStatus =
                            GetLocalisedResource(Localization.ResourceId.MailSuccessfullyCompleted, languageCode);
                        break;
                    case TransactionStatus.Completed when hasWarnings:
                        status = $"<span style=\"color:green;font-weight:bold;\">{TransactionStatus.Completed}</span>";
                        subjectStatus =
                            GetLocalisedResource(Localization.ResourceId.MailCompletedWithWarnings, languageCode);
                        break;
                    case TransactionStatus.Failed:
                        subjectStatus =
                            GetLocalisedResource(Localization.ResourceId.MailFailed, languageCode);
                        status = $"<span style=\"color:red;font-weight:bold;\">{TransactionStatus.Failed}</span>";
                        break;
                    case TransactionStatus.Submitted:
                        break;
                    default:
                        status = $"<span style=\"color:grey;font-weight:bold;\">{TransactionStatus.Submitted}</span>";
                        break;
                }

                var subject = string.Format(
                    GetLocalisedResource(Localization.ResourceId.MailSubject, languageCode),
                    subjectStatus,
                    transactionResult.Dataflow,
                    transactionResult.DestinationDataSpaceId,
                    transactionResult.TransactionId
                );

                var summary = string.Format(
                    GetLocalisedResource(
                        Localization.ResourceId.EmailSummary, languageCode),
                    transactionResult.TransactionId,
                    emailSummarySourceDataSpace,
                    transactionResult.DataSource,
                    transactionResult.DestinationDataSpaceId,
                    transactionResult.Dataflow,
                    transactionResult.User,
                    status
                );

                var body = string.Format(
                    GetLocalisedResource(Localization.ResourceId.MailBody, languageCode), 
                    summary,
                    transactionMessages
                    );


                var mailMessage = new MailMessage(
                    @from: _configuration.MailFrom,
                    to: transactionResult.User,
                    subject: subject,
                    body: body)
                {
                    IsBodyHtml = true
                };

                smtpClient.Send(mailMessage);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }
    }
}