﻿using DotStat.Common.Logger;
using log4net.Core;

namespace DotStatServices.Transfer
{
    public enum TransactionType
    {
        Import,
        Transfer,
        PitRollback,
        PitRestore
    }
    public enum TransactionStatus
    {
        Submitted,
        InProgress,
        Completed,
        Failed
    }

    public class TransactionResult
    {

        public TransactionType TransactionType { get; set; }
        public int TransactionId { get; set; }
        public string SourceDataSpaceId { get; set; }
        public string DestinationDataSpaceId { get; set; }
        public string Dataflow { get; set; }
        public string User { get; set; }
        public TransactionStatus TransactionStatus { get; set; }
        public string DataSource { get; set; }

        public LoggingEvent[] TransactionLogs => LogHelper.GetRecordedEvents(TransactionId, DestinationDataSpaceId);

    }
}