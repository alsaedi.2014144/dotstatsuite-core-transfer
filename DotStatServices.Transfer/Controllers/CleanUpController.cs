﻿using DotStat.Common.Auth;
using DotStat.Common.Configuration;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Localization;
using DotStat.Common.Logger;
using DotStat.Db.Manager;
using DotStat.Domain;
using DotStat.MappingStore;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace DotStatServices.Transfer.Controllers
{
    /// <summary>
    /// CleanUp controller
    /// </summary>
    [ApiVersion("1.2")]
    public class CleanUpController : ControllerBase
    {
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly IAuthConfiguration _authConfiguration;
        private readonly IAuthorizationManagement _authorizationManagement;
        private readonly IMappingStoreDataAccess _mappingStoreDataAccess;
        private readonly BaseConfiguration _configuration;
        private readonly IDbManager _dbManager;

        /// <summary>
        /// Constructor for this controller filled by DryIoc
        /// </summary>
        public CleanUpController(
            IHttpContextAccessor contextAccessor,
            IAuthConfiguration authConfiguration,
            IAuthorizationManagement authorizationManagement,
            IMappingStoreDataAccess mappingStoreDataAccess, 
            BaseConfiguration configuration,
            IDbManager dbManager)
        {
            _contextAccessor = contextAccessor;
            _authConfiguration = authConfiguration;
            _authorizationManagement = authorizationManagement;
            _mappingStoreDataAccess = mappingStoreDataAccess;
            _configuration = configuration;
            _dbManager = dbManager;
        }

        /// <summary>
        /// Deletes everything from the data db for the provided dsd
        /// </summary>
        /// <param name="dataspace">Dataspace</param>
        /// <param name="dsd">AGENCYID:DSDID(VERSION)</param>
        /// <response code="200">Cleanup succeeded</response>
        /// <response code="400">Validation or Internal error</response>
        [Microsoft.AspNetCore.Mvc.HttpDelete]
        [Microsoft.AspNetCore.Mvc.Route("{version:apiVersion}/cleanup/dsd")]
        public ActionResult<OperationResult> CleanUpDsd(
            [FromForm, Required] string dataspace,
            [FromForm, Required] string dsd)
        {
            try
            {
                var dataStructure = dsd.GetDsd();

                if (!_authorizationManagement.IsAuthorized(
                    new DotStatPrincipal(_contextAccessor.HttpContext.User, _authConfiguration.ClaimsMapping),
                    dataspace,
                    dataStructure.AgencyId,
                    dataStructure.Id,
                    dataStructure.Version,
                    PermissionType.CanDeleteStructuralMetadata))
                {
                    return new ForbidResult();
                }

                var mappingStoreObjects = _mappingStoreDataAccess.GetDsd(
                    dataspace, 
                    dataStructure.AgencyId,
                    dataStructure.Id, 
                    dataStructure.Version);

                if (mappingStoreObjects.DataStructures.Any())
                {
                    return new OperationResult(false, string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.CleanUpErrorDsdStillExists), dsd));
                }

                var managementRepository = _dbManager.GetManagementRepository(dataspace.GetSpaceInternal(_configuration).Id);

                return managementRepository.CleanUpDsd(dataStructure.AgencyId, dataStructure.Id, new SdmxVersion(dataStructure.Version)) ? 
                    new OperationResult(true, string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.CleanUpSuccessful), dsd)) :
                    new OperationResult(false, string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.CleanUpErrorNothingToDelete), dsd));
            }
            catch (Exception exception)
            {
                Log.Error(exception);
                throw;
            }
        }


    }
}
