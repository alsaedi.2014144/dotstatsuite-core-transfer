﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.IO;
using System.Threading.Tasks;
using DotStat.Common.Auth;
using DotStat.Common.Configuration;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Localization;
using DotStat.Db.Repository;
using DotStat.Domain;
using DotStat.Common.Logger;
using DotStat.Db.Manager;
using DotStat.Transfer.Manager;
using DotStat.Transfer.Param;
using DotStatServices.Transfer.BackgroundJob;
using DotStatServices.Transfer.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Org.Sdmxsource.Util.Url;
using static DotStat.Common.Localization.LocalizationRepository;

namespace DotStatServices.Transfer.Controllers
{
    /// <summary>
    /// Import controller
    /// </summary>
    [ApiVersion("1.2")]
    public class ImportController : ControllerBase
    {
        private readonly IDbManager _dbManager;
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly IAuthorizationManagement _authorizationManagement;
        private readonly ITransferManager<ExcelToSqlTransferParam> _excelTransferManager;
        private readonly ITransferManager<SdmxFileToSqlTransferParam> _sdmxFileTransferManager;
        private readonly ITransferManager<UrlToSqlTransferParam> _transferMngrUrl;
        private readonly IMailService _mailService;
        private readonly BackgroundQueue _backgroundQueue;
        private readonly BaseConfiguration _configuration;
        private readonly IAuthConfiguration _authConfiguration;

        /// <summary>
        /// Constructor for this Controlled filled by DryIoc
        /// </summary>
        public ImportController(
                IDbManager dbManager,
                IHttpContextAccessor contextAccessor,
                IAuthorizationManagement authorizationManagement,
                ITransferManager<ExcelToSqlTransferParam> excelTransferManager, 
                ITransferManager<SdmxFileToSqlTransferParam> sdmxFileTransferManager,
                ITransferManager<UrlToSqlTransferParam> transferMngrUrl,
                BaseConfiguration configuration,
                IAuthConfiguration authConfiguration,
                IMailService mailService,
                BackgroundQueue backgroundQueue
            )
        {
            _dbManager = dbManager;
            _contextAccessor = contextAccessor;
            _authorizationManagement = authorizationManagement;
            _excelTransferManager = excelTransferManager;
            _sdmxFileTransferManager = sdmxFileTransferManager;
            _transferMngrUrl = transferMngrUrl;
            _configuration = configuration;
            _authConfiguration = authConfiguration;
            _mailService = mailService;
            _backgroundQueue = backgroundQueue;
        }

        /// <summary>
        /// Initiates import of data from EDD + Excel files into provided data space
        /// </summary>
        /// <param name="dataspace">Dataspace</param>
        /// <param name="lang">Language code</param>
        /// <param name="eddFile">Edd file</param>
        /// <param name="excelFile">Excel file</param>
        /// <param name="targetVersion">Target table version LIVE or PIT</param>
        /// <param name="PITReleaseDate">Point in time release date</param>
        /// <param name="restorationOptionRequired">Keep current LIVE version for restoration after PIT release</param>
        /// <returns></returns>
        /// <response code="200">Import task successfully submitted</response>
        /// <response code="400">Validation or Internal error</response>        
        [Microsoft.AspNetCore.Mvc.HttpPost]
        [Microsoft.AspNetCore.Mvc.Route("{version:apiVersion}/import/excel")]
        public ActionResult<OperationResult> ImportExcel(
            [FromForm, Required] string dataspace,
            [FromForm] string lang,
            [Required] IFormFile eddFile,
            [Required] IFormFile excelFile,
            [FromForm] TargetVersion? targetVersion,
            [FromForm] string PITReleaseDate,
            [FromForm] Boolean restorationOptionRequired)
        {
            //set LIVE version as default
            if (targetVersion == null)
                targetVersion = TargetVersion.Live;
            
            if (targetVersion != TargetVersion.PointInTime)
            {
                PITReleaseDate = null;
                restorationOptionRequired = false;
            }

            var destinationDataspace = dataspace.GetSpaceInternal(_configuration);
            var sqlManagementRepository = _dbManager.GetManagementRepository(destinationDataspace.Id);

            var transferParam = new ExcelToSqlTransferParam()
            {
                DestinationDataspace = destinationDataspace,
                EddFilePath = eddFile.GetLocalPath("eddFile", "xml"),
                ExcelFilePath = excelFile.GetLocalPath("excelFile", "xlsx"),
                CultureInfo = CultureInfo.GetCultureInfo(lang ?? _configuration.DefaultLanguageCode),
                TargetVersion = (TargetVersion)targetVersion,
                PITReleaseDate = PITReleaseDate.GetPITReleaseDate(),
                PITRestorationAllowed = restorationOptionRequired
            };

            transferParam.DataSource =
                string.Format(GetLocalisedResource(Localization.ResourceId.EmailSummaryDataSourceEddExcel, transferParam.CultureInfo.TwoLetterISOLanguageName),
                    eddFile.FileName, excelFile.FileName);

            return Import(_excelTransferManager, transferParam, sqlManagementRepository);
        }


        /// <summary>
        /// Initiates import of data from SDMX file (xml|csv) into provided data space
        /// </summary>
        /// <param name="dataspace">Dataspace</param>
        /// <param name="lang">Language code</param>
        /// <param name="dataflow">AGENCYID:DATAFLOWID(VERSION)</param>
        /// <param name="file">SDMX CSV / XML file</param>
        /// <param name="targetVersion">Target table version LIVE or PIT</param>
        /// <param name="PITReleaseDate">Point in time release date</param>
        /// <param name="restorationOptionRequired">Keep current LIVE version for restoration after PIT release</param>
        /// <returns></returns>
        /// <response code="200">Import task successfully submitted</response>
        /// <response code="400">Validation or Internal error</response>
        [Microsoft.AspNetCore.Mvc.HttpPost]
        [Microsoft.AspNetCore.Mvc.Route("{version:apiVersion}/import/sdmxFile")]
        public ActionResult<OperationResult> ImportSdmxFile(
            [FromForm, Required] string dataspace,
            [FromForm, Required] string dataflow,
            [FromForm] string lang,
            IFormFile file,
            [FromForm] string filepath,
            [FromForm] TargetVersion? targetVersion,
            [FromForm] string PITReleaseDate,
            [FromForm] Boolean restorationOptionRequired
            )
        {
            //set LIVE version as default
            if (targetVersion == null)
                targetVersion = TargetVersion.Live;
            
            if (targetVersion != TargetVersion.PointInTime)
            {
                PITReleaseDate = null;
                restorationOptionRequired = false;
            }

            var destinationDataspace = dataspace.GetSpaceInternal(_configuration);
            var sqlManagementRepository = _dbManager.GetManagementRepository(destinationDataspace.Id);

            ActionResult<OperationResult> requestMsg;

            //import from sdmx source
            if (Extensions.IsValidUrl(filepath))
            {
                Extensions.IsAccessible(filepath);

                var transferParam = new UrlToSqlTransferParam()
                {
                    SourceDataflow = dataflow.GetDataflow(),
                    DestinationDataspace = destinationDataspace,
                    Url = filepath.ToUri(),
                    CultureInfo = CultureInfo.GetCultureInfo(lang ?? _configuration.DefaultLanguageCode),
                    TargetVersion = (TargetVersion)targetVersion,
                    PITReleaseDate = PITReleaseDate.GetPITReleaseDate(),
                    PITRestorationAllowed = restorationOptionRequired,
                    DataSource = filepath
                };

                requestMsg = Import(_transferMngrUrl, transferParam, sqlManagementRepository);

            }
            //import from sdmx file
            else
            {
                var transferParam = new SdmxFileToSqlTransferParam()
                {
                    DestinationDataspace = destinationDataspace,
                    CultureInfo = CultureInfo.GetCultureInfo(lang ?? _configuration.DefaultLanguageCode),
                    SourceDataflow = dataflow.GetDataflow(),
                    TargetVersion = (TargetVersion)targetVersion,
                    PITReleaseDate = PITReleaseDate.GetPITReleaseDate(),
                    PITRestorationAllowed = restorationOptionRequired
                };

                if (string.IsNullOrEmpty(filepath))
                {
                    transferParam.FilePath = file.GetLocalPath("file", "csv", "xml", "zip"); 
                    transferParam.Extension = Path.GetExtension(file.FileName);
                    transferParam.DataSource = file.FileName;
                }
                else
                {
                    transferParam.FilePath = filepath;
                    transferParam.Extension = Path.GetExtension(filepath);
                    transferParam.DataSource = filepath; 
                }

                requestMsg = Import(_sdmxFileTransferManager, transferParam, sqlManagementRepository);
            }
            return requestMsg;
        }


        private ActionResult<OperationResult> Import<T>(ITransferManager<T> transferManager, T transferParam, IManagementRepository managementRepository) where T : TransferParam
        {
            try
            {
                transferParam.Id = managementRepository.GetNextTransactionId();
                transferParam.Principal = new DotStatPrincipal(_contextAccessor.HttpContext.User, _authConfiguration.ClaimsMapping);

                var dataflow = transferManager.Producer.GetDataflow(transferParam);

                var transactionResult = new TransactionResult()
                {
                    TransactionType = TransactionType.Import,
                    TransactionId = transferParam.Id,
                    DataSource = transferParam.DataSource,
                    SourceDataSpaceId = transferParam.SourceDataspace?.Id,
                    DestinationDataSpaceId = transferParam.DestinationDataspace?.Id,
                    Dataflow = $"{dataflow?.AgencyId}:{dataflow?.Base.Id}({dataflow?.Version})",
                    User = transferParam.Principal.Email,
                    TransactionStatus = TransactionStatus.Submitted
                };
                
                LogHelper.RecordNewTransaction(transferParam.Id, transferParam.DestinationDataspace);
                Log.SetTransactionId(transferParam.Id);
                Log.SetDataspaceId(transferParam.DestinationDataspace?.Id);

                var message = string.Format(
                    LocalizationRepository.GetLocalisedResource(
                        Localization.ResourceId.SubmissionResult,
                        transferParam.CultureInfo.TwoLetterISOLanguageName),
                    transferParam.Id);

                Log.Notice(message);

                if(!IsAuthorized(transferManager, transferParam))
                    return new ForbidResult();

                _backgroundQueue.Enqueue(async cancellationToken =>
                {
                    Log.SetTransactionId(transferParam.Id);
                    Log.SetDataspaceId(transferParam.DestinationDataspace.Id);
                    try
                    {
                        transactionResult.TransactionStatus = TransactionStatus.InProgress;
                        transferManager.Transfer(transferParam);
                        transactionResult.TransactionStatus = TransactionStatus.Completed;
                    }
                    catch(Exception exception)
                    {
                        transactionResult.TransactionStatus = TransactionStatus.Failed;
                        Log.Warn(LocalizationRepository.GetLocalisedResource(
                            Localization.ResourceId.NoObservationsProcessed,
                            transferParam.CultureInfo.TwoLetterISOLanguageName));
                        Log.Error(exception);
                    }
                    finally
                    {
                        _mailService.SendMail(
                                transactionResult,
                                transferParam.CultureInfo.TwoLetterISOLanguageName
                            );
                    }

                    await Task.CompletedTask;
                });

                return new OperationResult(true, message);
            }
            catch (Exception exception)
            {
                Log.Error(exception);

                throw;
            }
        }

        private bool IsAuthorized<T>(ITransferManager<T> transferManager, T transferParam) where T : TransferParam
        {
            var dataflow = transferManager.Producer.GetDataflow(transferParam);

            return _authorizationManagement.IsAuthorized(
                transferParam.Principal,
                transferParam.DestinationDataspace.Id,
                dataflow.AgencyId,
                dataflow.Base.Id,
                dataflow.Version.ToString(),
                PermissionType.CanImportData
            );
        }
    }
}
