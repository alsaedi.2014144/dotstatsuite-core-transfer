﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using DotStat.Common.Configuration;
using DotStatServices.Transfer.HealthCheck;
using DotStat.Common.Logger;
using DryIoc;
using DryIoc.Microsoft.DependencyInjection;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using IHostingEnvironment = Microsoft.AspNetCore.Hosting.IHostingEnvironment;

namespace DotStatServices.Transfer
{
    [ExcludeFromCodeCoverage]
    public class Startup
    {
        private readonly AuthConfiguration _auth;

        public Startup(IConfiguration configuration)
        {
            _auth = configuration.GetSection("auth").Get<AuthConfiguration>();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            IdentityModelEventSource.ShowPII = true;

            // By default, Microsoft has some legacy claim mapping that converts standard JWT claims into proprietary ones.
            // This removes those mappings.
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            BackgroundJob.BackgroundTask.Register(services);

            services
                .AddMvc(options =>
                {
                    if (_auth?.Enabled == true)
                    {
                        options.Filters.Add(new AuthorizeFilter());
                    }
                    
                    options.Filters.Add(new ProducesAttribute("application/json"));
                    options.Filters.Add(new ProducesResponseTypeAttribute(typeof(OperationResult), (int) HttpStatusCode.OK));
                    options.Filters.Add(new ProducesResponseTypeAttribute(typeof(OperationResult), (int) HttpStatusCode.BadRequest));
                    options.Filters.Add(new ProducesResponseTypeAttribute((int) HttpStatusCode.Unauthorized));
                    options.Filters.Add(new ProducesResponseTypeAttribute((int) HttpStatusCode.Forbidden));
                })
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddJsonOptions(options =>
                    options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver());
            
            services.AddApiVersioning();

            // -----------------------------------------------------------------------

            if (_auth?.Enabled == true)
            {
                services.AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

                }).AddJwtBearer(options =>
                {
                    options.Authority = _auth.Authority;
                    options.Audience = _auth.ClientId;

                    options.RequireHttpsMetadata = _auth.RequireHttps;
                    options.TokenValidationParameters = new TokenValidationParameters()
                    {
                        ValidateIssuer = _auth.ValidateIssuer
                    };
                });
            }

            // -----------------------------------------------------------------------

            //Allow HttpContext to be accessed by external library (log4net logHelper.cs)
            services.AddHttpContextAccessor();

            //This line adds Swagger generation services to our container.
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1.2", new Info { Title = "Transfer service", Version = "1.2" });

                if (_auth?.Enabled == true)
                {
                    c.AddSecurityDefinition("oauth2", new TransferOAuth2Scheme
                    {
                        XTokenName = "id_token",
                        Flow = "implicit",
                        AuthorizationUrl = _auth.AuthorizationUrl,
                        Scopes = _auth.Scopes.ToDictionary(x=>x,x=>x)
                    });
                    c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>>()
                    {
                        { "oauth2", _auth.Scopes}
                    });
                }

                c.DocInclusionPredicate((docName, apiDesc) =>
                {
                    if (!apiDesc.TryGetMethodInfo(out MethodInfo methodInfo))
                        return false;

                    var versions = methodInfo.DeclaringType
                        .GetCustomAttributes(true)
                        .OfType<ApiVersionAttribute>()
                        .SelectMany(attr => attr.Versions);

                    return versions.Any(v => $"v{v.ToString()}" == docName);
                });

                //Locate the XML file being generated by ASP.NET...
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);

                //... and tell Swagger to use those XML comments.
                c.IncludeXmlComments(xmlPath);
            });

            //https://docs.microsoft.com/en-us/aspnet/core/host-and-deploy/health-checks?view=aspnetcore-2.2
            services.AddHealthChecks()
                .AddCheck<ServiceHealthCheck>("service", tags: new[] { "live" })
                .AddCheck<DbHealthCheck>("database")
                .AddCheck<MemoryHealthCheck>("memory");

            services.AddCors();

            // Dry IOC setup
            return new Container()
                .WithDependencyInjectionAdapter(services)
                .ConfigureServiceProvider<ApiIoc>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            // Global exception handler
            var exceptionMiddleware = new JsonExceptionMiddleware(env);
            app.UseExceptionHandler(new ExceptionHandlerOptions
            {
                ExceptionHandler = exceptionMiddleware.Invoke
            });

            if (_auth?.Enabled == true)
            {
                app.UseAuthentication();
            }

            Log.Configure(app.ApplicationServices.GetRequiredService<IHttpContextAccessor>());

            app.UseCors(BuildCorsPolicy);
            app.UseMvc();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.DocumentTitle = "Transfer service UI";
                c.SwaggerEndpoint("./v1.2/swagger.json", "Transfer service v1.2");

                if (_auth?.Enabled == true)
                {
                    c.OAuthClientId(_auth.ClientId);
                   
                    // This is hack to request "id_token" instead of hardcoded in Swagger UI "token"
                    c.HeadContent = @"<script>if (!window.isOpenReplaced) {
                        window.open = function(open) {
                            return function(url) {
                                url = url.replace('response_type=token', 'response_type=id_token');
                                url += '&nonce=' + new Date().getTime();
                                return open.call(window, url);
                            };
                        } (window.open);
                        window.isOpenReplaced = true;
                    }
                    </script>";
                }
            });

            app.UseHealthChecks("/health", new DotStatHealthCheckOptions());
            app.UseHealthChecks("/live", new DotStatHealthCheckOptions()
            {
                Predicate = (x) => x.Tags.Contains("live")
            });
        }

        private void BuildCorsPolicy(CorsPolicyBuilder builder)
        {
            //todo: make configurable

            builder
                    .WithOrigins("http://localhost", "http://127.0.0.1") // this is needed to add Vary header
                    .SetIsOriginAllowed(o => true)
                    .AllowAnyHeader()
                    .AllowAnyMethod()
                    .AllowCredentials();
        }

        // This is hack class to instruct Swagger sending "id_token" as Bearer instead of default "access_token"
        public class TransferOAuth2Scheme : OAuth2Scheme
        {
            [JsonProperty("x-tokenName")]
            public string XTokenName { get; set; }
        }
    }
}
