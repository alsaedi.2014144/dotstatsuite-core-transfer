﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Engine;
using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
using Org.Sdmxsource.Sdmx.Api.Model.Objects;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
using Org.Sdmxsource.Sdmx.Api.Util;
using Org.Sdmxsource.Sdmx.DataParser.Engine.Reader;
using Org.Sdmxsource.Sdmx.DataParser.Manager;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure;
using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
using Org.Sdmxsource.Sdmx.Util.Objects.Container;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
using Org.Sdmxsource.Util.Io;
using Attribute = DotStat.Domain.Attribute;

namespace DotStat.Transfer.Test.Unit
{
    public static class TestDataHelper
    {
        public static ISdmxObjects GetSdmxObjects(string filename)
        {
            ISdmxObjects sdmxObjects = new SdmxObjectsImpl();
            IStructureParsingManager structureParsingManager = new StructureParsingManager();
            using (IReadableDataLocation dataLocation = new FileReadableDataLocation(filename))
            {
                sdmxObjects.Merge(structureParsingManager.ParseStructures(dataLocation).GetStructureObjects(false));
            }

            return sdmxObjects;
        }
        
        public static TransferContent GetTransferContent(string filename, Dataflow dataflow)
        {
            var keyables = new List<IKeyable>();
            var datasetAttributes = new List<IKeyValue>();

            var content = new TransferContent()
            {
                Keyables = keyables,
                DatasetAttributes = datasetAttributes
            };

            var manager = new DataReaderManager();
            ReadableDataLocationFactory dataLocationFactory = new ReadableDataLocationFactory();

            using (var sourceData = dataLocationFactory.GetReadableDataLocation(new FileStream(filename, FileMode.Open)))
            {
                using (var dataReaderEngine = manager.GetDataReaderEngine(sourceData, dataflow.Dsd.Base, null))
                {
                    content.Observations = Read(dataReaderEngine, keyables, datasetAttributes);
                }
            }

            return content;
        }
        
        private static IEnumerable<IObservation> Read(IDataReaderEngine reader, List<IKeyable> keyables, List<IKeyValue> datasetAttributes)
        {
            keyables.Clear();
            datasetAttributes.Clear();

            IList<IObservation> obs = new List<IObservation>();

            var abstractReader = reader as AbstractDataReaderEngine;

            if (reader.MoveNextDataset())
            {
                datasetAttributes.AddRange(reader.DatasetAttributes);

                var observationAttributesOfDsd = reader.DataStructure.ObservationAttributes;
                var dimGroupAttributesOfDsd = reader.DataStructure.Attributes.Where(a =>
                    a.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup || a.AttachmentLevel == AttributeAttachmentLevel.Group);

                while (reader.MoveNextKeyable())
                {
                    var currentKeyable = reader.CurrentKey;

                    var observationAttributeKeyValues = new List<IKeyValue>();

                    keyables.Add(currentKeyable);

                    if (reader.CurrentKey.Series && reader.MoveNextObservation())
                    {
                        do
                        {
                            var currentObservation = reader.CurrentObservation;

                            obs.Add(currentObservation);
                        }

                        while (reader.MoveNextObservation());
                    }
                }
            }

            return obs;
        }
        
        public static IDataflowMutableObject CreateDataflowMutableObject(IDataStructureMutableObject dsd)
        {
            IDataflowMutableObject df = new DataflowMutableCore();
            df.AgencyId = "SDMXSOURCE";
            df.Id = "df_id";
            df.AddName("en", "name");
            df.DataStructureRef = dsd.ImmutableInstance.AsReference;
            return df;
        }

        public static IDataStructureMutableObject CreateDataStructureMutableObject()
        {
            IDataStructureMutableObject dsd = new DataStructureMutableCore();
            dsd.AgencyId = "SDMXSOURCE";
            dsd.Id = "WDI";
            dsd.AddName("en", "World Development Indicators");

            var concept = new StructureReferenceImpl("SDMXSOURCE", "CONCEPTS", "1.0", SdmxStructureEnumType.Concept, "COUNTRY");
            var codelist = new StructureReferenceImpl("SDMXSOURCE", "CL_COUNTRY", "1.0", SdmxStructureEnumType.CodeList);
            dsd.AddDimension(concept, codelist);

            var time = new StructureReferenceImpl("SDMXSOURCE", "CONCEPTS", "1.0", SdmxStructureEnumType.Concept, "TIME");
            IDimensionMutableObject timeDim = dsd.AddDimension(time, null);
            timeDim.TimeDimension = true;

            var obsVal =
                new StructureReferenceImpl("SDMXSOURCE", "CONCEPTS", "1.0", SdmxStructureEnumType.Concept, "OBS_VALUE");
            dsd.AddPrimaryMeasure(obsVal);
            return dsd;
        }
    }

    /// <summary>
    /// Add Dimensions and Attributes
    /// </summary>
    public static class DsdExtender
    {
        /// <summary>
        /// Complete dimensions info for a DSD
        /// </summary>
        /// <param name="dsd"></param>
        /// <param name="dims"></param>
        public static void SetDimensions(this Dsd dsd, IEnumerable<IDimension> dims)
        {
            List<Dimension> dimensions = new List<Dimension>();

            foreach (var d in dims)
            {
                var dimAdd = new Dimension(d.Position, d, null);
                dimensions.Add(dimAdd);
            }

            dsd.SetDimensions(dimensions);
        }

        /// <summary>
        /// Complete attributes info for a DSD
        /// </summary>
        /// <param name="dsd"></param>
        /// <param name="attributes"></param>
        public static void SetAttributes(this Dsd dsd, IEnumerable<IAttributeObject> attributes)
        {
            List<Attribute> attribs = new List<Attribute>();

            foreach (var a in attributes)
            {
                var dimAdd = new Attribute(a);
                attribs.Add(dimAdd);
            }

            dsd.SetAttributes(attribs);
        }
    }


}
