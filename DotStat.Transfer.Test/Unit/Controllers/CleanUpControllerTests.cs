﻿using DotStat.Common.Auth;
using DotStat.Common.Configuration;
using DotStat.Common.Configuration.Dto;
using DotStat.Db.Manager;
using DotStat.Db.Repository;
using DotStat.Domain;
using DotStat.MappingStore;
using DotStat.Test;
using DotStatServices.Transfer;
using DotStatServices.Transfer.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Objects;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
using Org.Sdmxsource.Sdmx.Util.Extension;
using Org.Sdmxsource.Sdmx.Util.Objects.Container;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;

namespace DotStat.Transfer.Test.Unit.Controllers
{
    [TestFixture]
    public class CleanUpControllerTests : SdmxUnitTestBase
    {
        private CleanUpController _controller;
        private readonly AuthConfiguration _authConfig;
        private readonly IDataStructureObject _dsd;
        private readonly Mock<IHttpContextAccessor> _contextAccessorMock = new Mock<IHttpContextAccessor>();
        private readonly Mock<IAuthorizationManagement> _authManagementMock = new Mock<IAuthorizationManagement>();
        private readonly string _dataSpace = "design";
        private readonly Mock<IMappingStoreDataAccess> _mappingStoreDataAccessMock = new Mock<IMappingStoreDataAccess>();
        private readonly Mock<IDbManager> _dbManagerMock = new Mock<IDbManager>();
        private readonly Mock<IManagementRepository> _managementRepositoryMock = new Mock<IManagementRepository>();


        public CleanUpControllerTests()
        {
            _contextAccessorMock
                .Setup(x => x.HttpContext.User)
                .Returns(new ClaimsPrincipal(new ClaimsIdentity()));

            _authConfig = new AuthConfiguration()
            {
                ClaimsMapping = new Dictionary<string, string>()
            };

            var sdmxObjects = new SdmxObjectsImpl(DatasetActionEnumType.Append);
            sdmxObjects.Merge(new FileInfo("sdmx/264D_264_SALDI+2.1.xml").GetSdmxObjects(new StructureParsingManager()));
            _dsd = sdmxObjects.DataStructures.First();

            _authManagementMock
                .Setup(x => x.IsAuthorized(
                    It.IsAny<DotStatPrincipal>(),
                    _dataSpace,
                    _dsd.AgencyId,
                    _dsd.Id,
                    _dsd.Version.ToString(),
                    PermissionType.CanDeleteStructuralMetadata
                ))
                .Returns(true);

            _dbManagerMock
                .Setup(x => x.GetManagementRepository(It.IsAny<string>()))
                .Returns(_managementRepositoryMock.Object);

            Configuration.SpacesInternal = new List<DataspaceInternal>()
            {
                new DataspaceInternal() {Id = _dataSpace}
            };
        }

        [Test]
        public void CleanUpDsdWithWrongDsd()
        {
            _controller = new CleanUpController(_contextAccessorMock.Object, _authConfig, _authManagementMock.Object, _mappingStoreDataAccessMock.Object, Configuration, _dbManagerMock.Object);

            var exception = Assert.Throws<ArgumentException>(() => _controller.CleanUpDsd("design", "wrong ID"));
            Assert.That(exception.Message.Contains("AGENCYID:ID(VERSION)", StringComparison.OrdinalIgnoreCase));
        }

        [Test]
        public void CleanUpDsdUnauthorized()
        {
            _controller = new CleanUpController(_contextAccessorMock.Object, _authConfig, _authManagementMock.Object, _mappingStoreDataAccessMock.Object, Configuration, _dbManagerMock.Object);

            var result = _controller.CleanUpDsd("design", "NOACCESS:264D_Consumer_Confiance(1.0)");
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Result);
            Assert.IsInstanceOf<ForbidResult>(result.Result);
        }

        [Test]
        public void CleanUpDsdMappingStoreHasDsd()
        {
            SetupMappingStoreDataAccess(new SdmxObjectsImpl(null, _dsd));
            _controller = new CleanUpController(_contextAccessorMock.Object, _authConfig, _authManagementMock.Object, _mappingStoreDataAccessMock.Object, Configuration, _dbManagerMock.Object);

            var result = _controller.CleanUpDsd("design", "IT1:264D_Consumer_Confiance(1.0)");
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Value);
            Assert.IsInstanceOf<OperationResult>(result.Value);
            Assert.IsFalse(result.Value.Success);
        }

        [Test]
        public void CleanUpDsdNothingToDelete()
        {
            SetupMappingStoreDataAccess(new SdmxObjectsImpl());
            SetupManagementRepository(false);
            _controller = new CleanUpController(_contextAccessorMock.Object, _authConfig, _authManagementMock.Object, _mappingStoreDataAccessMock.Object, Configuration, _dbManagerMock.Object);

            var result = _controller.CleanUpDsd("design", "IT1:264D_Consumer_Confiance(1.0)");
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Value);
            Assert.IsInstanceOf<OperationResult>(result.Value);
            Assert.IsFalse(result.Value.Success);
        }

        [Test]
        public void CleanUpDsdSuccessful()
        {
            SetupMappingStoreDataAccess(new SdmxObjectsImpl());
            SetupManagementRepository(true);
            _controller = new CleanUpController(_contextAccessorMock.Object, _authConfig, _authManagementMock.Object, _mappingStoreDataAccessMock.Object, Configuration, _dbManagerMock.Object);

            var result = _controller.CleanUpDsd("design", "IT1:264D_Consumer_Confiance(1.0)");
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Value);
            Assert.IsInstanceOf<OperationResult>(result.Value);
            Assert.IsTrue(result.Value.Success);
        }

        [TearDown]
        public void TearDown()
        {
            _authManagementMock.Invocations.Clear();
        }

        private void SetupMappingStoreDataAccess(ISdmxObjects dsd)
        {
            _mappingStoreDataAccessMock.Setup(x =>
                    x.GetDsd(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(dsd);
        }

        private void SetupManagementRepository(bool result)
        {
            _managementRepositoryMock.Setup(x => x.CleanUpDsd(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<SdmxVersion>())).Returns(result);
        }
    }
}
