﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Auth;
using DotStat.Common.Configuration;
using DotStat.Common.Configuration.Dto;
using DotStat.Db.Manager;
using DotStat.Db.Repository;
using DotStat.Domain;
using DotStat.MappingStore;
using DotStat.Test;
using DotStat.Transfer.Manager;
using DotStatServices.Transfer.BackgroundJob;
using DotStatServices.Transfer.Controllers;
using DotStatServices.Transfer.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

namespace DotStat.Transfer.Test.Unit.Controllers
{
    [TestFixture]
    public class PitControllerTests : SdmxUnitTestBase
    {
        private readonly PointInTimeController _controller;
        private readonly Mock<IHttpContextAccessor> _contextAccessorMock = new Mock<IHttpContextAccessor>();
        private readonly Mock<IMappingStoreDataAccess> _mappingStore = new Mock<IMappingStoreDataAccess>();
        private readonly Mock<IAuthorizationManagement> _authManagement = new Mock<IAuthorizationManagement>();
        private readonly Mock<IMailService> _mailService = new Mock<IMailService>();
        private readonly Mock<IDbManager> _dbManager = new Mock<IDbManager>();
        private readonly Mock<ITransactionRepository> _transactionRepo = new Mock<ITransactionRepository>();
        private readonly Mock<IManagementRepository> _managementRepo = new Mock<IManagementRepository>();
        private readonly Mock<BackgroundQueue> _backgroundQueueMock = new Mock<BackgroundQueue>();
        private readonly string _dataSpace = "design";
        private readonly Dataflow _dataflow;
        private readonly AuthConfiguration _authConfig;

        public PitControllerTests()
        {
            this.Configuration.SpacesInternal = new List<DataspaceInternal>()
            {
                new DataspaceInternal() {Id = _dataSpace}
            };
            this.Configuration.DefaultLanguageCode = "en";

            _dataflow = GetDataflow();

            _transactionRepo.Setup(mock => mock.GetDSDPITInfo(It.IsAny<Dataflow>())).Returns(new JObject());
            _transactionRepo.Setup(mock => mock.TryNewTransaction(It.IsAny<int>(), It.IsAny<Dataflow>(),
                It.IsAny<TargetVersion>(), It.IsAny<bool>())).Returns(true);
            _transactionRepo.Setup(mock => mock.Rollback(It.IsAny<Dataflow>(), It.IsAny<IMappingStoreDataAccess>()));

            _authManagement
                .Setup(x => x.IsAuthorized(
                    It.IsAny<DotStatPrincipal>(),
                    _dataSpace,
                    _dataflow.AgencyId,
                    _dataflow.Code,
                    _dataflow.Version.ToString(),
                    PermissionType.CanImportData
                ))
                .Returns(true);


            _dbManager.Setup(mock => mock.GetTransactionRepository(It.IsAny<string>()))
                .Returns(_transactionRepo.Object);

            _mappingStore.Setup(mock =>
                    mock.GetDataflow(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>()))
                .Returns(new Dataflow(_dataflow.Base, _dataflow.Dsd));

            _managementRepo.Setup(mock => mock.DeleteTransactionItem(It.IsAny<int>(), It.IsAny<bool>()));
            _dbManager.Setup(mock => mock.GetManagementRepository(It.IsAny<string>())).Returns(_managementRepo.Object);

            _contextAccessorMock
                .Setup(x => x.HttpContext.User)
                .Returns(new ClaimsPrincipal(new ClaimsIdentity()));

            _backgroundQueueMock
                .Setup(x => x.Enqueue(It.IsAny<Func<CancellationToken, Task>>()))
                .Callback((Func<CancellationToken, Task> task) =>
                {
                    task.Invoke(new CancellationToken());
                });

            _authConfig = new AuthConfiguration()
            {
                ClaimsMapping = new Dictionary<string, string>()
            };

            var commonManager = new CommonManager(_mappingStore.Object, _authManagement.Object, Configuration);

            _controller = new PointInTimeController(
                _dbManager.Object,
                _contextAccessorMock.Object,
                commonManager, 
                Configuration,
                _authConfig,
                _mailService.Object,
                _backgroundQueueMock.Object
             );
        }

        [Test]
        public void PitInfoTest()
        {
            var result = _controller.PitInfo("design", _dataflow.FullId);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Result);
            Assert.IsInstanceOf<OkObjectResult>(result.Result);
        }

        [Test]
        public void RollbackTest()
        {
            var result = _controller.Rollback("design", _dataflow.FullId);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Result);
            Assert.IsInstanceOf<OkObjectResult>(result.Result);
        }

        [Test]
        public void RestoreTest()
        {
            var result = _controller.Restore("design", _dataflow.FullId);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Result);
            Assert.IsInstanceOf<OkObjectResult>(result.Result);
        }
    }
}
