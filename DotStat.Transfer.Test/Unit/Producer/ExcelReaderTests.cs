﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using DotStat.Domain;
using DotStat.Test.Moq;
using DotStat.Transfer.Excel.Excel;
using DotStat.Transfer.Excel.Mapping;
using DotStat.Transfer.Excel.Reader;
using DotStat.Transfer.Param;
using DotStat.Transfer.Producer;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Model.Data;

namespace DotStat.Transfer.Test.Unit.Producer
{
    [TestFixture]
    public class ExcelReaderTests
    {
        [TestCase("sdmx/264D_264_SALDI2_ATTRIBUTE_TEST.xml", "edd/264D_264_SALDI-all-attributes.xml", "excel/264D_264_SALDI-all-attributes.xlsx", 100, 4, 20)]
        public void TestExcelProducer(
            string structurePath, 
            string eddPath, 
            string excelPath, 
            int expectedObservationCount, 
            int expectedDatasetAttrCount, 
            int expectedDimAttrCount
        )
        {
            var prm = new ExcelToSqlTransferParam()
            {
                EddFilePath = eddPath,
                ExcelFilePath = excelPath
            };

            var mappingStoreDataAccess = new TestMappingStoreDataAccess(structurePath);
            var producer = new ExcelProducer(mappingStoreDataAccess);
            var dataflow = producer.GetDataflow(prm);
            var result = producer.Process(prm, dataflow);

            Assert.IsNotNull(result.Observations);
            Assert.AreEqual(expectedObservationCount, result.Observations.Count());

            Assert.IsNotNull(result.DatasetAttributes);
            Assert.AreEqual(expectedDatasetAttrCount, result.DatasetAttributes.Count());

            Assert.IsNotNull(result.Keyables);
            Assert.AreEqual(expectedDimAttrCount, result.Keyables.Count());
        }

        [Test]
        public void ReadDatasetLvlAttributes()
        {
            var descriptor = new ExcelDatasetAttributesDescriptor("Dataset-lvl-attributes", new[]
            {
                new DatasetAttributeLookup("COLLECTION", "D6"),
                new DatasetAttributeLookup("TIME_FORMAT", "D7"),
                new DatasetAttributeLookup("TEST_ATTR_DATASET_CODED", "D8"),
                new DatasetAttributeLookup("TEST_ATTR_DATASET_CODED2", "D9"),
            });

            var msAccess    = new TestMappingStoreDataAccess("sdmx/264D_264_SALDI2_ATTRIBUTE_TEST.xml");
            var dataflow    = msAccess.GetDataflow();
            var excel       = new EPPlusExcelDataSource("excel/264D_264_SALDI-all-attributes.xlsx");
            var iterator    = new ExcelCellDatasetAttrIterator(descriptor, dataflow, excel);
            var reader      = new SWCanonicalReader<IKeyValue>(iterator, dataflow);
            var attrCount   = 0;

            var expectedAttributes = new Dictionary<string, string>
            {
                {"COLLECTION", "A"},
                {"TIME_FORMAT", "P1Y"},
                {"TEST_ATTR_DATASET_CODED", "P6M"},
                {"TEST_ATTR_DATASET_CODED2", "PT1M"}
            };

            while (reader.MoveNext())
            {
                attrCount++;

                Assert.IsNotNull(reader.Current);
                Assert.IsTrue(expectedAttributes.ContainsKey(reader.Current.Concept));
                Assert.AreEqual(expectedAttributes[reader.Current.Concept], reader.Current.Code);
            }

            Assert.AreEqual(4, attrCount);
        }

        [Test]
        public void ReadDimensionLvlAttributes()
        {
            var dimensionCells = new[]
            {
                new SdmxArtifactCellReferenceExpression("FREQ", null, new [] {"Cell(Row, 2)"}, null),
                new SdmxArtifactCellReferenceExpression("REF_AREA", null, new [] {"Cell(Row, 3)"}, null),
                new SdmxArtifactCellReferenceExpression("IND_TYPE", null, new [] {"Cell(Row, 4)"}, null),
                new SdmxArtifactCellReferenceExpression("ADJUSTMENT", null, new [] {"Cell(Row, 5)"}, null),
                new SdmxArtifactCellReferenceExpression("MEASURE", null, new [] {"Cell(Row, 6)"}, null),
                new SdmxArtifactCellReferenceExpression("ETA", null, new [] {"Cell(Row, 7)"}, null),
                new SdmxArtifactCellReferenceExpression("SESSO", null, new [] {"Cell(Row, 8)"}, null),
                new SdmxArtifactCellReferenceExpression("GRADO_ISTRUZ", null, new [] {"Cell(Row, 9)"}, null),
                new SdmxArtifactCellReferenceExpression("CATEG_PROF", null, new [] {"Cell(Row, 10)"}, null),
            };

            var descriptor = new ExcelDimAttributesDescriptor(
                new[] {new MatchPattern("Dim-lvl-attributes", false)},
                new CellReferenceExpression("K4", null),
                new CellReferenceExpression("LastNonEmptyCell(\"K4:ZZ10000\")", null, true),
                new SdmxArtifactCellReferenceExpression("-", null, new[] { "Cell(3, Column)" }, null), 
                dimensionCells
            );

            // --------------------------------------------------------------------------------------

            var msAccess    = new TestMappingStoreDataAccess("sdmx/264D_264_SALDI2_ATTRIBUTE_TEST.xml");
            var dataflow    = msAccess.GetDataflow();
            var excel       = new EPPlusExcelDataSource("excel/264D_264_SALDI-all-attributes.xlsx");

            var iterator    = new IteratorChain<ExcelCellDimensionAttrIterator, IKeyable>(descriptor.BuildIterators(excel, dataflow));
            var reader      = new SWCanonicalReader<IKeyable>(iterator, dataflow);
            var attrCount   = 0;

            var expectedAttributes = new Dictionary<string, Dictionary<string,string>>
            {
                {"METADATA_IT", new Dictionary<string, string>()
                {
                    {"A:ITF11:COF:C:1:Y0-14:1:1:1","attribute value 1"},   
                    {"A:066001:COF:C:1:Y0-14:1:1:1","attribute value 2"},   
                    {"A:066002:COF:C:1:Y0-14:1:1:1","attribute value 3"},   
                    {"A:066003:COF:C:1:Y0-14:1:1:1","attribute value 4"},   
                }},
                {"TITLE", new Dictionary<string, string>()
                {
                    {"A:C","some title for A:C"},
                    {"A:N","some title for A:N"},
                    {"A:S","some title for A:S"},
                    {"A:T","some title for A:T"},
                }},
                {"METADATA_EN", new Dictionary<string, string>()
                {
                    {"A:C","some metadata for A:C"},
                    {"A:N","some metadata for A:N"},
                    {"A:S","some metadata for A:S"},
                    {"A:T","some metadata for A:T"},
                }},
                {"TEST_ATTR_DIM_CODED", new Dictionary<string, string>()
                {
                    {"A:C","P1Y"},
                    {"A:N","P6M"},
                    {"A:S","P3M"},
                    {"A:T","P1M"},
                }},
                {"TEST_ATTR_GROUP_CODED", new Dictionary<string, string>()
                {
                    {"A:C","P1Y"},
                    {"A:N","P6M"},
                    {"A:S","P3M"},
                    {"A:T","P1M"},
                }},
            };

            while (reader.MoveNext())
            {
                attrCount++;

                Assert.IsNotNull(reader.Current);
                Assert.IsNotNull(reader.Current.Key);
                Assert.AreEqual(1, reader.Current.Attributes.Count);
                
                var attribute = reader.Current.Attributes.First();

                Assert.IsTrue(expectedAttributes.ContainsKey(attribute.Concept));
                
                var coords = expectedAttributes[attribute.Concept];
                var key = string.Join(":", reader.Current.Key.Select(k => k.Code));

                Assert.IsTrue(coords.ContainsKey(key));
                Assert.AreEqual(coords[key], attribute.Code);
            }

            Assert.AreEqual(20, attrCount);
        }

        [TestCase("TITLE:Test value:A:::C:::::", 2, "Test value", null)]
        [TestCase("METADATA_EN:metadata sample:A:::C:::::", 2, "metadata sample", "IdentifiedGroup")]
        [TestCase("TEST_ATTR_DIM_CODED:Semi-Annual:A:::N:::::", 2, "P6M", null)]
        [TestCase("TEST_ATTR_GROUP_CODED:Monthly:A:::T:::::", 2, "P1M", "IdentifiedGroup")]
        [TestCase("METADATA_IT:attribute value 1:A:ITF11:COF:C:1:Y0-14:1:1:1", 9, "attribute value 1", null)]
        public void DimensionAttrMapper(string values, int expectedDimCount, string expectedAttrValue, string expectedGroupId)
        {
            var msAccess    = new TestMappingStoreDataAccess("sdmx/264D_264_SALDI2_ATTRIBUTE_TEST.xml");
            var dataflow    = msAccess.GetDataflow();
            var mapper      = new GroupAttrMapper(dataflow);
            var arr         = values.Split(':');

            var key         = mapper.Transform(arr);

            Assert.IsNotNull(key);
            Assert.AreEqual(expectedDimCount, key.Key.Count);
            Assert.AreEqual(expectedAttrValue, key.Attributes.First().Code);
            Assert.AreEqual(expectedGroupId, key.GroupName);
        }

        [TestCase(null, "excel/264D_264_SALDI.xlsx", 100)]
        [TestCase("edd/264D_264_SALDI-no-attributes.xml", "excel/264D_264_SALDI.xlsx", 100)]
        [TestCase("edd/264D_264_SALDI-pivot-table.xml", "excel/264D_264_SALDI.xlsx", 300)]
        [TestCase("edd/264D_264_SALDI-en-labels.xml", "excel/264D_264_SALDI.xlsx", 300)]
        [TestCase("edd/264D_264_SALDI-it-labels.xml", "excel/264D_264_SALDI.xlsx", 300)]
        public void ExcelReader(string eddPath, string excelPath, int expectedObservationCount)
        {
            var reader = GetExcelReader(new TestMappingStoreDataAccess(), eddPath, excelPath);

            var obsVal = 0m;
            var obsCount = 0;

            while (reader.MoveNext())
            {
                obsCount++;

                Assert.IsNotNull(reader.Current);
                Assert.AreEqual(9, reader.Current.SeriesKey.Key.Count);
                Assert.IsTrue(decimal.TryParse(reader.Current.ObservationValue, out obsVal));
            }

            Assert.AreEqual(expectedObservationCount, obsCount);
        }

        private SWReaderBase<IObservation> GetExcelReader(TestMappingStoreDataAccess msAccess, string eddPath, string excelPath)
        {
            var dataflow    = msAccess.GetDataflow();
            var excelSource = new EPPlusExcelDataSource(excelPath);

            var description = !string.IsNullOrEmpty(eddPath)
                ? DescriptionFromXml(eddPath, msAccess)
                : DescriptionFromObject(dataflow);

            var iterator = description.GetObservationCellIterator(excelSource);

            return new SWCanonicalReader<IObservation>(iterator, dataflow);
        }

        private ExcelDataDescription DescriptionFromXml(string eddPath, TestMappingStoreDataAccess msAccess)
        {
            return ExcelDataDescription.Build(1, "Test name", "Test description", XDocument.Load(eddPath), msAccess, "");
        }

        private ExcelDataDescription DescriptionFromObject(Dataflow dataflow)
        {
            return new ExcelDataDescription(1, "Test name", "Test description", dataflow, new[]
            {
                GetEddDescription(dataflow)
            });
        }

        private ExcelCoordMappingDescriptor GetEddDescription(Dataflow dataflow)
        {
            var worksheets = new[]
            {
                new MatchPattern("Observations", false)
            };

            var dimensionCells = new[]
            {
                new SdmxArtifactCellReferenceExpression("TIME_PERIOD", new[] {"Cell(Row, Column-10).Value"}, null, null),
                new SdmxArtifactCellReferenceExpression("FREQ", new[] {"Cell(Row, Column-9).Value"}, null, null),
                new SdmxArtifactCellReferenceExpression("REF_AREA", new[] {"Cell(Row, Column-8).Value"}, null, null),
                new SdmxArtifactCellReferenceExpression("IND_TYPE", new[] {"Cell(Row, Column-7).Value"}, null, null),
                new SdmxArtifactCellReferenceExpression("ADJUSTMENT", new[] {"Cell(Row, Column-6).Value"}, null, null),
                new SdmxArtifactCellReferenceExpression("MEASURE", new[] {"Cell(Row, Column-5).Value"}, null, null),
                new SdmxArtifactCellReferenceExpression("ETA", new[] {"Cell(Row, Column-4).Value"}, null, null),
                new SdmxArtifactCellReferenceExpression("SESSO", new[] {"Cell(Row, Column-3).Value"}, null, null),
                new SdmxArtifactCellReferenceExpression("GRADO_ISTRUZ", new[] {"Cell(Row, Column-2).Value"}, null, null),
                new SdmxArtifactCellReferenceExpression("CATEG_PROF", new[] {"Cell(Row, Column-1).Value"}, null, null),
            };

            var coord = new ExcelCoordMappingDescriptor(
                "Test",
                dataflow,
                worksheets,
                topLeft: new CellReferenceExpression("M6", null),
                bottomRight: new CellReferenceExpression("M105", null),
                dimensionCells: dimensionCells
            )
            {
                IsActive = true
            };

            return coord;
        }
    }
}