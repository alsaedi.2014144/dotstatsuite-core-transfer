﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using DotStat.Common.Auth;
using DotStat.Common.Configuration;
using DotStat.Common.Configuration.Dto;
using DotStat.Common.Localization;
using DotStat.Db.Repository;
using DotStat.Db.Util;
using DotStat.Db.Validation;
using DotStat.Db.Validation.SqlServer;
using DotStat.Domain;
using DotStat.MappingStore;
using DotStat.Test;
using DotStat.Transfer.Consumer;
using DotStat.Transfer.Param;
using DryIoc;
using Microsoft.Extensions.Configuration;
using Moq;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using Org.Sdmxsource.Sdmx.Api.Model.Objects;

namespace DotStat.Transfer.Test.Unit.Consumer
{
    [TestFixture]
    public class SqlConsumerTest : SdmxUnitTestBase
    {
        private readonly Mock<IMappingStoreDataAccess> _mappingstore = new Mock<IMappingStoreDataAccess>();
        private readonly Mock<IManagementRepository> _mgmtRepository = new Mock<IManagementRepository>();
        private readonly Mock<IDataStoreRepository> _dataStoreRepository = new Mock<IDataStoreRepository>();
        private readonly Mock<ITransactionRepository> _transRepository = new Mock<ITransactionRepository>();
        private readonly Mock<IAuthorizationManagement> _authorisation = new Mock<IAuthorizationManagement>();
        private readonly Mock<ISqlDatasetAttributeDatabaseValidator> _sqlDatasetAttributeDatabaseValidator = new Mock<ISqlDatasetAttributeDatabaseValidator>();
        private readonly Mock<ISqlKeyableDatabaseValidator> _keyableAttributeDatabaseValidator = new Mock<ISqlKeyableDatabaseValidator>();
        private readonly Mock<IDatasetAttributeValidator> _datasetAttributeValidator = new Mock<IDatasetAttributeValidator>();
        private readonly Mock<ICodeTranslator> _codeTranslator = new Mock<ICodeTranslator>();
        private readonly Mock<ISqlTransferParam> _param = new Mock<ISqlTransferParam>();

        private readonly Dataflow _dataflow;
        private readonly TransferContent _content;
        private readonly SqlConsumer _sqlConsumer;

        public SqlConsumerTest()
        {
            this.Configuration.MaxTransferErrorAmount = 0;

            List<IValidationError> _emptyErrorList = new List<IValidationError>();

            _dataStoreRepository.Setup(x => x.BulkInsertData(It.IsAny<IEnumerable<IObservation>>(),
                It.IsAny<CodeTranslator>(), It.IsAny<Dataflow>(), out _emptyErrorList));

            _dataflow = base.GetDataflow();

            _mappingstore.Setup(x =>
                    x.GetDataflow(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>()))
                .Returns(_dataflow);

            _param.Setup(exp => exp.Principal)
                .Returns(new DotStatPrincipal(new ClaimsPrincipal(), new Dictionary<string, string>()));

            _param.Setup(exp => exp.DestinationDataspace).Returns(new DataspaceInternal { Id = "DummySpace" });
            _param.Setup(exp => exp.DestinationDataflow).Returns(_dataflow.Base.MutableInstance);
            _param.Setup(exp => exp.CultureInfo).Returns(CultureInfo.CurrentUICulture);

            _transRepository.Setup(x => x.TryNewTransaction(It.IsAny<int>(), It.IsAny<Dataflow>(), It.IsAny<TargetVersion>(), It.IsAny<bool>())).Returns(true);

            _authorisation.Setup(x => x.IsAuthorized(
                It.IsAny<DotStatPrincipal>(),
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<PermissionType>())
            ).Returns(true);

            _sqlDatasetAttributeDatabaseValidator.Setup(x => x.Validate(
                It.IsAny<DbTableVersion>(),
                It.IsAny<IEnumerable<IKeyValue>>(),
                It.IsAny<IEnumerable<IKeyValue>>(),
                It.IsAny<Dataflow>(),
                It.IsAny<int>())).Returns(true);

            _sqlDatasetAttributeDatabaseValidator.Setup(x => x.GetErrors()).Returns(new List<IValidationError>());

            _keyableAttributeDatabaseValidator.Setup(x => x.GetErrors()).Returns(new List<IValidationError>());

            _datasetAttributeValidator.Setup(x => x.GetErrors()).Returns(new List<IValidationError>());

            _content = new TransferContent()
            {
                Observations = ObservationGenerator.Generate(_dataflow, 2010, 2020, 100),
                DatasetAttributes = Enumerable.Empty<IKeyValue>(),
                Keyables = Enumerable.Empty<IKeyable>()
            };

            _sqlConsumer = new SqlConsumer(_mappingstore.Object, _authorisation.Object, this.Configuration);
        }


        [Test]
        public void SaveLiveVersionData()
        {
            _transRepository.Setup(x => x.TryNewTransaction(It.IsAny<int>(), It.IsAny<Dataflow>(), It.IsAny<TargetVersion>(), It.IsAny<bool>())).Returns(true);
            
            _mgmtRepository.Reset();

            _param.Setup(exp => exp.TargetVersion).Returns(TargetVersion.Live);

            bool success = _sqlConsumer.Save(
                _param.Object,
                _dataflow,
                _content,
                _mgmtRepository.Object,
                _transRepository.Object,
                _dataStoreRepository.Object,
                _sqlDatasetAttributeDatabaseValidator.Object,
                _keyableAttributeDatabaseValidator.Object,
                _datasetAttributeValidator.Object,
                _codeTranslator.Object
                );

            _authorisation.Verify(mock => mock.IsAuthorized(
                            It.IsAny<DotStatPrincipal>(),
                            "DummySpace",
                            _dataflow.AgencyId,
                            _dataflow.Code,
                            _dataflow.Version.ToString(),
                            PermissionType.CanImportData
                        ),
                        Times.Once
                    );
            
            _mgmtRepository.Verify(m=>m.CheckManagementTables(_param.Object.Id));
            _transRepository.Verify(m =>
                m.TryNewTransaction(It.IsAny<int>(), It.IsAny<Dataflow>(), It.IsAny<TargetVersion>(),
                    It.IsAny<bool>()));

            //Check that  live version was called and not PIT
            _transRepository.Verify(
                m => m.ApplyPITRelease(It.IsAny<Dataflow>(), It.IsAny<bool>(), It.IsAny<int>(),
                    It.IsAny<IMappingStoreDataAccess>()), Times.Never);

           Assert.IsTrue(success);
        }
        
        [Test]
        public void ExecutePitRelease()
        {
            _mgmtRepository.Setup(exp => exp.GetDsdPITVersion(_dataflow.Dsd)).Returns(DbTableVersion.A);
            _mgmtRepository.Setup(exp => exp.GetDsdPITReleaseDate(_dataflow.Dsd)).Returns(DateTime.MinValue);
            _mgmtRepository.Setup(exp => exp.GetDsdLiveVersion(_dataflow.Dsd)).Returns(DbTableVersion.A);

            _param.Setup(exp => exp.TargetVersion).Returns(TargetVersion.PointInTime);
           
            bool success = _sqlConsumer.Save(
                _param.Object,
                _dataflow,
                _content,
                _mgmtRepository.Object,
                _transRepository.Object,
                _dataStoreRepository.Object,
                _sqlDatasetAttributeDatabaseValidator.Object,
                _keyableAttributeDatabaseValidator.Object,
                _datasetAttributeValidator.Object,
                _codeTranslator.Object
            );
            
            _transRepository.Verify(
                m => m.ApplyPITRelease(It.IsAny<Dataflow>(), It.IsAny<bool>(), It.IsAny<int>(),
                    It.IsAny<IMappingStoreDataAccess>()), Times.Once);

            Assert.IsTrue(success);
        }

        [Test]
        public void CopyDataToPitVersion()
        {
            _param.Setup(exp => exp.TargetVersion).Returns(TargetVersion.PointInTime);

            bool success = _sqlConsumer.Save(
                _param.Object,
                _dataflow,
                _content,
                _mgmtRepository.Object,
                _transRepository.Object,
                _dataStoreRepository.Object,
                _sqlDatasetAttributeDatabaseValidator.Object,
                _keyableAttributeDatabaseValidator.Object,
                _datasetAttributeValidator.Object,
                _codeTranslator.Object
            );

            _dataStoreRepository.Verify(
                m => m.CopyAttributesToNewVersion(It.IsAny<Dsd>(), It.IsAny<DbTableVersion>()), Times.Once);

            Assert.IsTrue(success);
        }
        
        [Test]
        public new void GetDataflow()
        {
            var df = _sqlConsumer.GetDataflow(_param.Object);

            _mappingstore.Verify(x=> x.GetDataflow(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>()), Times.Once);
            
            Assert.AreEqual(_dataflow.FullId, df.FullId);

        }

        [TearDown]
        public void TearDown()
        {
            _transRepository.Invocations.Clear();
            _mgmtRepository.Invocations.Clear();
            _authorisation.Invocations.Clear();
        }
    }
}
